package org.buffalocoder.quanlyquancafe.client.controller;

import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.buffalocoder.quanlyquancafe.common.entities.Ban;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.interfaces.IBanDAO;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.common.utils.Validate;

import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

public class ModifierBanController implements Initializable {
    private double x, y;
    private AlertController alertController;
    private IBanDAO banDAO;

    @FXML public TableView<Ban> tblBan;
    @FXML private JFXTextField txtMaBan, txtTongChoNgoi;
    @FXML private StackPane stackPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);
        try {
            banDAO = Services.getInstance().getBanService();

            txtMaBan.setText(banDAO.generateId());
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    // region Events
    /**
     * Phương thức đóng scene
     *
     * @param actionEvent
     */
    @FXML
    private void onClose(ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.close();
    }

    /**
     * Phương thức kéo thả scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onDragged(MouseEvent mouseEvent) {
        Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        stage.setX(mouseEvent.getScreenX() - x);
        stage.setY(mouseEvent.getScreenY() - y);
    }

    /**
     * Phương thức xử lý khi nhấn vào scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onPressed(MouseEvent mouseEvent) {
        x = mouseEvent.getSceneX();
        y = mouseEvent.getSceneY();
    }

    /**
     * Phương thức xử lý khi nhấn button thêm bàn
     * @param actionEvent
     * @throws RemoteException
     * @throws NotBoundException
     */
    @FXML
    public void onClickThemBan(ActionEvent actionEvent) {
        if (!validate()) return;

        String maBan = txtMaBan.getText().trim();
        int tongChoNgoi = Integer.parseInt(txtTongChoNgoi.getText().trim());

        try {
            Ban ban = new Ban(maBan, false, tongChoNgoi);

            if (banDAO.add(ban)) {
                if(tblBan != null){
                    tblBan.getItems().add(ban);
                    alertController.alertInfo("Thêm bàn thành công");
                }
            }
        } catch (ValidateException | RemoteException e) {
            alertController.alertError(e.getMessage());
        } finally {
            onClose(actionEvent);
        }
    }
    // endregion

    // region Methods
    public void setTableBan(TableView<Ban> tblBan) {
        this.tblBan = tblBan;
        try {
            txtMaBan.setText(banDAO.generateId());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {
        if (!Validate.isNumber(txtTongChoNgoi.getText().trim())) {
            alertController.alertError("Vui lòng nhập tổng chỗ ngồi là số");
            return false;
        }

        return true;
    }
    // endregion
}


