package org.buffalocoder.quanlyquancafe.client.controller;

import com.jfoenix.controls.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.common.entities.DiaChi;
import org.buffalocoder.quanlyquancafe.common.entities.DienThoai;
import org.buffalocoder.quanlyquancafe.common.entities.MonAn;
import org.buffalocoder.quanlyquancafe.common.entities.NhanVien;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.interfaces.INhanVienDAO;
import org.buffalocoder.quanlyquancafe.common.types.GioiTinhEnum;
import org.buffalocoder.quanlyquancafe.common.types.LoaiDienThoaiEnum;
import org.buffalocoder.quanlyquancafe.common.types.LoaiNhanVienEnum;

import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

public class ModifierNhanVienController implements Initializable {
    public Label lblTitle;
    private double x, y;
    private AlertController alertController;
    private TableView<NhanVien> tblNhanVien;
    private NhanVien nhanVienSelected;
    private INhanVienDAO nhanVienDAO;
    private boolean isEdit;

    @FXML private StackPane stackPane;
    @FXML private JFXTextField txtTenNhanVien, txtMaNhanVien, txtEmail, txtCMND, txtSoDienThoai;
    public JFXComboBox<String> comboBoxLoaiNhanVien;
    @FXML private JFXTextField txtDiaChiChiTiet, txtKhuVuc, txtQuanHuyen, txtTinhThanhPho;
    @FXML private JFXPasswordField txtMatKhau;
    @FXML private JFXDatePicker dateNgaySinh, dateNgayVaoLam;
    @FXML private JFXRadioButton radioNam, radioNu;
    @FXML private JFXButton btnSave;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);
        this.isEdit = false;

        comboBoxLoaiNhanVien.getItems().add("Nhân viên");
        comboBoxLoaiNhanVien.getItems().add("Admin");

        try {
            nhanVienDAO = Services.getInstance().getNhanVienService();
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    // region Methods
    /**
     * Phương thức nhận danh sách nhân viên từ scene cha
     * @param tblNhanVien
     */
    public void setTableNhanVien(TableView<NhanVien> tblNhanVien) {
        this.tblNhanVien = tblNhanVien;
        try {
            txtMaNhanVien.setText(nhanVienDAO.generateId());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức nhận nhân viên được chọn từ scene cha
     * @param nhanVienSelected
     */
    public void setNhanVienSelected(NhanVien nhanVienSelected) {
        this.nhanVienSelected = nhanVienSelected;
        this.isEdit = true;
        this.btnSave.setText("Lưu");
        this.lblTitle.setText("Cập nhật thông tin nhân viên");

        txtMaNhanVien.setText(nhanVienSelected.getMaNhanVien());
        txtTenNhanVien.setText(nhanVienSelected.getTenNhanVien());
        txtEmail.setText(nhanVienSelected.getEmail());
        dateNgaySinh.setValue(nhanVienSelected.getNgaySinh());
        dateNgayVaoLam.setValue(nhanVienSelected.getNgayVaoLam());
        txtDiaChiChiTiet.setText(nhanVienSelected.getDiaChi().getDiaChiChiTiet());
        txtKhuVuc.setText(nhanVienSelected.getDiaChi().getKhuVuc());
        txtQuanHuyen.setText(nhanVienSelected.getDiaChi().getQuanHuyen());
        txtTinhThanhPho.setText(nhanVienSelected.getDiaChi().getTinhThanhPho());
        txtCMND.setText(nhanVienSelected.getChungMinhNhanDan());
        txtSoDienThoai.setText(nhanVienSelected.getDienThoai().getSoDienThoai());
        radioNam.setSelected(nhanVienSelected.getGioiTinh().equals("Nam"));
        radioNu.setSelected(nhanVienSelected.getGioiTinh().equals("Nữ"));
        comboBoxLoaiNhanVien.setValue(nhanVienSelected.getLoaiNhanVien() == LoaiNhanVienEnum.NHAN_VIEN ? "Nhân viên" : "Admin");
    }

    /**
     * Phương thức xử lý thêm nhân viên
     * @param actionEvent
     * @param nhanVien
     */
    private void themNhanVien(NhanVien nhanVien, ActionEvent actionEvent) throws RemoteException {
        if (nhanVienDAO.add(nhanVien)) {

            if (tblNhanVien != null) {
                tblNhanVien.getItems().add(nhanVien);
                onClose(actionEvent);
                alertController.alertInfo("Thêm nhân viên thành công");
            }
        } else alertController.alertError("Không thể thêm nhân viên vào lúc này");
    }

    /**
     * Phương thức xử lý cập nhật nhân viên
     * @param actionEvent
     * @param nhanVien
     */
    private void capNhatNhanVien(NhanVien nhanVien, ActionEvent actionEvent) throws RemoteException {
        if (nhanVienDAO.update(nhanVien)) {
            if (tblNhanVien != null) {
                onClose(actionEvent);
                int index = tblNhanVien.getItems().indexOf(nhanVienSelected);
                tblNhanVien.getItems().set(index, nhanVien);
                alertController.alertInfo("Cập nhật thông tin nhân viên thành công");
            }
        } else alertController.alertError("Không thể cập nhật thông tin nhân viên vào lúc này");
    }

    /**
     * Phương thức kiểm tra dữ liệu
     * @return boolean
     */
    private boolean validate() {
        return true;
    }
    // endregion

    // region Events

    /**
     * Phương thức đóng scene
     *
     * @param actionEvent
     */
    @FXML
    private void onClose(javafx.event.ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.close();
    }

    /**
     * Phương thức kéo thả scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onDragged(MouseEvent mouseEvent) {
        Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        stage.setX(mouseEvent.getScreenX() - x);
        stage.setY(mouseEvent.getScreenY() - y);
    }

    /**
     * Phương thức xử lý khi nhấn vào scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onPressed(MouseEvent mouseEvent) {
        x = mouseEvent.getSceneX();
        y = mouseEvent.getSceneY();
    }

    /**
     * Sự kiện khi nhấn nút Lưu
     * @param actionEvent
     */
    public void onClickLuu(javafx.event.ActionEvent actionEvent) {
        if (!validate()) return;

        try {
            NhanVien nhanVien = new NhanVien(
                    txtMaNhanVien.getText().trim(),
                    txtTenNhanVien.getText().trim(),
                    txtMatKhau.getText().trim(),
                    GioiTinhEnum.KHONG_XAC_DINH,
                    dateNgaySinh.getValue(),
                    txtEmail.getText().trim(),
                    dateNgayVaoLam.getValue(),
                    LoaiNhanVienEnum.NHAN_VIEN,
                    txtCMND.getText().trim(),
                    new DiaChi(
                            txtDiaChiChiTiet.getText().trim(),
                            txtKhuVuc.getText().trim(),
                            txtQuanHuyen.getText().trim(),
                            txtTinhThanhPho.getText().trim()
                    ),
                    new DienThoai(
                            LoaiDienThoaiEnum.DI_DONG,
                            txtSoDienThoai.getText().trim()
                    )
            );

            if (isEdit) {
                capNhatNhanVien(nhanVien, actionEvent);
            } else {
                themNhanVien(nhanVien, actionEvent);
            }
        } catch (ValidateException | RemoteException e) {
            alertController.alertError(e.getMessage());
        }
    }
    // endregion
}
