package org.buffalocoder.quanlyquancafe.client.controller.nhanvien;

import com.jfoenix.controls.JFXDatePicker;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import org.buffalocoder.quanlyquancafe.client.controller.AlertController;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.common.consts.FieldNameConst;
import org.buffalocoder.quanlyquancafe.common.entities.HoaDon;
import org.buffalocoder.quanlyquancafe.common.interfaces.IHoaDonDAO;

import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class ThongKeController implements Initializable {
    @FXML
    JFXDatePicker dateThongKe;
    @FXML
    Label lblTienBanHang ,lblDonHang, lblTienThuVe, lblTongThu, lblTongDanhThuTatCa;
    private AlertController alertController;
    public IHoaDonDAO hoaDonDAO = Services.getInstance().getHoaDonService();
    @FXML private StackPane stackPane;
    @FXML private TableView tblHoaDon;
    @FXML private TableColumn<HoaDon, String> colMaHoaDon;
    @FXML private TableColumn<HoaDon, String> colMaBan;
    @FXML private TableColumn<HoaDon, String> colTenNhanVien;
    @FXML private TableColumn<HoaDon, Double> colTongTien;
    @FXML private TableColumn<HoaDon, LocalDate> colNgayXuatHoaDon;
    @FXML private TableColumn<HoaDon, String> colGhiChu;
    public ThongKeController() throws RemoteException, NotBoundException {
    }

    @FXML
    private void back(ActionEvent event) throws IOException {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
        openMain(stage);
    }
    private void openMain(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/nhanvien/MainScene.fxml"));
        root.getStylesheets().add(getClass().getResource("/css/common.css").toExternalForm());
        stage.setScene(new Scene(root, 1500, 840));
        stage.show();
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);

        initTable();

        try {
            List<HoaDon> hoaDons = hoaDonDAO.find();
            tblHoaDon.setItems(FXCollections.observableList(hoaDons));
        } catch (RemoteException e) {
            alertController.alertError(e.getMessage());
        }
    }

    // region Methods
    private void initTable() {
        colMaHoaDon.setCellValueFactory(new PropertyValueFactory<HoaDon, String>(FieldNameConst.HOADON_MAHOADON));
        colMaBan.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getBan().getMaBan()));
        colTenNhanVien.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getNhanVien().getTenNhanVien()));
        colTongTien.setCellValueFactory(cellData ->
                new SimpleObjectProperty<>(cellData.getValue().getTongTien()));
        colNgayXuatHoaDon.setCellValueFactory(new PropertyValueFactory<HoaDon, LocalDate>(FieldNameConst.HOADON_NGAYXUATHOADON));
        colGhiChu.setCellValueFactory(new PropertyValueFactory<HoaDon, String>(FieldNameConst.HOADON_GHICHU));
    }
    @FXML
    public void getTongDanhThu(ActionEvent event) throws RemoteException {
        if(dateThongKe.getValue() == null) {
            dateThongKe.setValue(LocalDate.now());
        }
        lblTienBanHang.setText(String.valueOf(hoaDonDAO.getTongDanhThu(dateThongKe.getValue())));
        lblDonHang.setText(String.valueOf(hoaDonDAO.getTongSoDonHang()));
        lblTienThuVe.setText(String.valueOf(hoaDonDAO.getTongTienThuVe()));
        lblTongThu.setText(String.valueOf(hoaDonDAO.getTongDanhThu(dateThongKe.getValue())));
        lblTongDanhThuTatCa.setText(String.valueOf(hoaDonDAO.getTongDoanhThuTatCa()));

    }
}
