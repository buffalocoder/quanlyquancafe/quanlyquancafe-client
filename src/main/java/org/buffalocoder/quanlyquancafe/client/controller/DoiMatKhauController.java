package org.buffalocoder.quanlyquancafe.client.controller;

import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.client.ultis.StorageAccount;
import org.buffalocoder.quanlyquancafe.common.entities.NhanVien;
import org.buffalocoder.quanlyquancafe.common.exceptions.NotExistException;
import org.buffalocoder.quanlyquancafe.common.interfaces.INhanVienDAO;

import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

/**
 * @author: Đặng Lê Minh Trường
 * @version: 1.0
 */
public class DoiMatKhauController implements Initializable {
    private AlertController alertController;
    private INhanVienDAO nhanVienDAO;

    @FXML private StackPane stackPane;
    @FXML private JFXTextField txtMatKhauCu, txtMatKhauMoi, txtNhapLaiMatKhauMoi;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);

        try {
            nhanVienDAO = Services.getInstance().getNhanVienService();
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    // region Methods
    @FXML
    private void back(ActionEvent event) throws IOException {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
        openMain(stage);
    }

    private boolean validate() {
        if (txtMatKhauCu.getText().trim().isEmpty()) {
            alertController.alertError("Vui lòng nhập mật khẩu hiện tại");
            return false;
        } else if (txtMatKhauMoi.getText().trim().isEmpty()) {
            alertController.alertError("Vui lòng nhập mật khẩu mới");
            return false;
        } else if (txtNhapLaiMatKhauMoi.getText().trim().isEmpty()) {
            alertController.alertError("Vui lòng nhập lại mật khẩu mới");
            return false;
        } else if (!txtMatKhauMoi.getText().trim().equals(txtNhapLaiMatKhauMoi.getText().trim())) {
            alertController.alertError("Mật khẩu mới và xác nhận không giống nhau");
            return false;
        }

        return true;
    }
    // endregion

    // region Events
    @FXML
    public void onCapNhat(ActionEvent actionEvent) {
        if (!validate()) return;

        String matKhauCu = txtMatKhauCu.getText().trim();
        String matKhauMoi = txtMatKhauMoi.getText().trim();

        try {
            NhanVien nhanVien = StorageAccount.getInstance().getNhanVien();

            boolean isCapNhat = nhanVienDAO.capNhatMatKhau(nhanVien.getMaNhanVien(), matKhauCu, matKhauMoi);
            alertController.alertInfo(isCapNhat ? "Đổi mật khẩu thành công" : "Đổi mật khẩu không thành công");
        } catch (Exception e) {
            alertController.alertError(e.getMessage());
        }

    }

    private void openMain(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/nhanvien/MainScene.fxml"));
        root.getStylesheets().add(getClass().getResource("/css/common.css").toExternalForm());
        stage.setScene(new Scene(root, 1500, 840));
        stage.show();
    }
    // endregion
}
