package org.buffalocoder.quanlyquancafe.client.controller.nhanvien;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import org.buffalocoder.quanlyquancafe.client.ultis.Component;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.common.entities.*;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.interfaces.IBanDAO;
import org.buffalocoder.quanlyquancafe.common.interfaces.IHoaDonDAO;
import org.buffalocoder.quanlyquancafe.common.types.GioiTinhEnum;
import org.buffalocoder.quanlyquancafe.common.types.LoaiDienThoaiEnum;
import org.buffalocoder.quanlyquancafe.common.types.LoaiNhanVienEnum;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class SoDoBanController implements Initializable {
    private List<Ban> banList;
    private Component component;
    private int row, column;
    private int index = 0;

    private HoaDon hoaDonBan;

    private IHoaDonDAO hoaDonDAO;

    private IBanDAO banDAO;
    @FXML
    private Pane pane_Tong;
    @FXML
    private GridPane GridPaneBan;

    private Set<ChiTietHoaDon> chiTietHoaDons;

    private GridPane GridPaneDatMon;

    private List< Set<ChiTietHoaDon>> Listsets = new ArrayList<>();

    private String maBanMuonThanhToan;

    private Set<String> chiTietMaBan;


    private List<String> chiTietMaBans;

    public String getMaBanMuonThanhToan() {
        return maBanMuonThanhToan;
    }


    public void setChiTietMaBans(List<String> chiTietMaBans) {
        this.chiTietMaBans = chiTietMaBans;
    }

    public void setChiTietMaBan(Set<String> chiTietMaBan) {
        this.chiTietMaBan = chiTietMaBan;
    }

    public void setMaBanMuonThanhToan(String maBanMuonThanhToan) {
        this.maBanMuonThanhToan = maBanMuonThanhToan;
    }

    public void setHoaDonBan(HoaDon hoaDonBan) {
        this.hoaDonBan = hoaDonBan;
    }

    public void setGridPaneDatMon(GridPane gridPaneDatMon) {
        GridPaneDatMon = gridPaneDatMon;
    }

    public void setChiTietHoaDons(Set<ChiTietHoaDon> chiTietHoaDons) {
        this.chiTietHoaDons = chiTietHoaDons;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        component = new Component();
        try {
            hoaDonDAO = Services.getInstance().getHoaDonService();
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }


        try {
            banDAO = Services.getInstance().getBanService();
            banList = banDAO.find();

            banList.forEach(ban -> {

                Set<ChiTietHoaDon> chiTietHoaDonsban = new HashSet<>();
                Listsets.add(chiTietHoaDonsban);
                //index++;

                Label labelBan = component.getBan(index+1, "lblBan" + String.valueOf(index+1),ban.isTrangThai());
                ///////////////////////////////////////////////////////////
                labelBan.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        //Parent root = null;
                        try {
                            FXMLLoader loader = new FXMLLoader();
                            loader.setLocation(getClass().getResource("/fxml/nhanvien/SodoMonan.fxml"));
                            Parent root = loader.load();
                            //int i=index;
                            //chiTietHoaDons=Listsets.get(i);
                            ///////////////////////////////////////////////////////////
                            Set<Node> deleteNodes = new HashSet<>(GridPaneDatMon.getChildren());
                            GridPaneDatMon.getChildren().removeAll(deleteNodes);


                            int vitriX = GridPaneBan.getColumnIndex(labelBan);
                            int vitriY=  GridPaneBan.getRowIndex(labelBan);

                            int vitri=(vitriY*4)+vitriX;
                            System.out.println(vitri);
                            System.out.println("**************************************");
//                            chiTietHoaDons=Listsets.get(vitri);
//                            Listsets.get(vitri).forEach(System.out::println);

                           HoaDon hoaDon = hoaDonDAO.findHoaDonChuaThanhToanByMaBan(banList.get(vitri).getMaBan());
                            maBanMuonThanhToan=banList.get(vitri).getMaBan();
                            //System.out.println("mã bàn muốn thanh toán");
                            System.out.println(maBanMuonThanhToan);
                            //chiTietMaBan=new HashSet<>();
                            chiTietMaBan.add(banList.get(vitri).getMaBan());
                            chiTietMaBans.add(banList.get(vitri).getMaBan());

                            chiTietMaBan = new HashSet<>();
                           if(hoaDon!=null){
                               hoaDon.getChiTietHoaDons().forEach(chiTietHoaDon1 -> {
                                   chiTietHoaDons.add(chiTietHoaDon1);
                                  // chiTietHoaDonBan.add(chiTietHoaDon1);
                               });
                               chiTietMaBan.add(hoaDon.getBan().getMaBan());
                               //maBanMuonThanhToan=banList.get(vitri).getMaBan();
                               System.out.println(hoaDon);
                               System.out.println("*************************");
                               System.out.println(hoaDon.getBan().getMaBan());
                               hoaDonBan=hoaDon;
                               System.out.println("*************************");
                               System.out.println(hoaDonBan);
                               System.out.println("*************************");
                               var ref = new Object() {
                                   int i = 0;
                               };
                               hoaDon.getChiTietHoaDons().forEach(pane->{
                                   try {
                                       Pane paneDatMon = component.getPaneDatMon(pane.getMonAn().getMaMonAn(),pane.getMonAn().getTenMonAn(),1,pane.getMonAn().getGiaBan(),pane.getMonAn().getGiaBan()*1);
                                       GridPaneDatMon.add(paneDatMon,0, ref.i);
                                   } catch (MalformedURLException e) {
                                       e.printStackTrace();
                                   }
                                   ref.i++;
                               });
                           }
                            System.out.println("-----------row--------------");
                           int vitriGrid=GridPaneDatMon.getChildren().size();
                            System.out.println(vitriGrid);
                            System.out.println("-------------------------");
                           if(hoaDon==null){
                               ban.setTrangThai(true);
                               banDAO.update( ban);


                               NhanVien nhanVien = new NhanVien(
                                       "NV0001",
                                       "Huynh Hieu Ngan",
                                       "12345678",
                                       GioiTinhEnum.NU,
                                       LocalDate.of(1999, 1, 1),
                                       "huynhhieungan@gmail.com",
                                       LocalDate.now(),
                                       LoaiNhanVienEnum.NHAN_VIEN,
                                       "345626115",
                                       new DiaChi(
                                               "12 Nguyen Van Bao",
                                               "Phuong 4",
                                               "Quan Go Vap",
                                               "Thanh pho Ho Chi Minh"
                                       ),
                                       new DienThoai(
                                               LoaiDienThoaiEnum.DI_DONG,
                                               "0945836291"
                                       )
                               );
                               System.out.println("chưa có hóa đơn");
                               Set<ChiTietHoaDon> chiTietHoaDons = new HashSet<>();
                               hoaDonBan = new HoaDon(hoaDonDAO.generateId(), LocalDate.now(), "", nhanVien, banList.get(vitri));
                               hoaDonBan.setChiTietHoaDons(chiTietHoaDons);
                              boolean a= hoaDonDAO.add(hoaDonBan);
                              if(a){
                                  System.out.println("true");
                              }else {
                                  System.out.println("false");
                              }
                           }


                            //chiTietHoaDonsban.forEach(System.out::println);

                            ////////////////////////////////////////////////////////////
                            SodoMonanController sodoMonanController =loader.getController();
                            sodoMonanController.setIndex(vitriGrid);
                            sodoMonanController.setChiTietHoaDons(chiTietHoaDons);
                            sodoMonanController.setChiTietHoaDonBan(Listsets.get(vitri));
                            sodoMonanController.setGridPaneDatMon(GridPaneDatMon);
                            sodoMonanController.setHoaDonban(hoaDonBan);
                            sodoMonanController.setMaBan(maBanMuonThanhToan);




                            pane_Tong.getChildren().removeAll();
                            pane_Tong.getChildren().setAll(root);
                        } catch (IOException | ValidateException e) {
                            e.printStackTrace();
                        }
                    }
                });

                ////////////////////////////////////////////////////////////
                GridPaneBan.add(labelBan, column, row);
                column++;
                if (column == 4) {
                    row++;
                    column = 0;
                }

                index++;

            });
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleButtonMouse(MouseEvent event) throws IOException {
    }
}
