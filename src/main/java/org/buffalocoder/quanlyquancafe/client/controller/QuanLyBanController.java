package org.buffalocoder.quanlyquancafe.client.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.common.entities.Ban;
import org.buffalocoder.quanlyquancafe.common.entities.DanhMucMonAn;
import org.buffalocoder.quanlyquancafe.common.interfaces.IBanDAO;

import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.ResourceBundle;

public class QuanLyBanController implements Initializable {
    private Scene scene;
    private Stage stage;
    private Parent root;
    private AlertController alertController;
    private ObservableList<Ban> banObservableList;
    private IBanDAO banDAO;

    @FXML private StackPane stackPane;
    @FXML private TextField txtTimKiem;
    @FXML public TableView<Ban> tblBan;
    @FXML private TableColumn<Ban, String> colId;
    @FXML private TableColumn<Ban, Integer> colSoGhe;
    @FXML private TableColumn<Ban, Boolean> colTrangThai;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);

        colId.setCellValueFactory(new PropertyValueFactory<Ban, String>("maBan"));
        colSoGhe.setCellValueFactory(new PropertyValueFactory<Ban, Integer>("tongChoNgoi"));
        colTrangThai.setCellValueFactory(new PropertyValueFactory<Ban, Boolean>("trangThai"));

        try {
            banDAO = Services.getInstance().getBanService();
            List<Ban> bans = banDAO.find();

            if (bans != null && bans.size() > 0) {
                setBanObservableList(FXCollections.observableList(bans));
            }
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    // region Methods
    public void setBanObservableList(ObservableList<Ban> banObservableList) {
        this.banObservableList = banObservableList;
        tblBan.setItems(banObservableList);
    }

    /**
     * Phương thức lấy item đang được chọn
     * @return Ban
     */
    private Ban getItemSelected() {
        Ban banSelected = null;

        try {
            banSelected = tblBan.getSelectionModel().getSelectedItem();
        } catch (Exception e) {
            alertController.alertError("Vui lòng chọn bàn cần xoá");
        }

        if (banSelected == null) {
            alertController.alertError("Vui lòng chọn bàn cần xoá");
        }

        return banSelected;
    }
    // endregion

    // region Events
    /**
     * Phương thức xử lý khi người dùng nhấn button xóa bàn
     * @param event
     */
    @FXML
    private void onClickXoaBan(ActionEvent event) {
        Ban banSelected = getItemSelected();

        if (banSelected == null) return;

        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Cảnh báo"));
        dialogLayout.setBody(new Text(String.format("Bạn có chắc muốn xoá bàn %s không?", banSelected.getMaBan())));
        JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btn_Ok = new JFXButton();
        btn_Ok.setText("Đồng ý");
        btn_Ok.setOnAction(e -> {
            try {
                System.out.println(banSelected);
                if (banDAO.deleteById(banSelected.getMaBan())) {
                    banObservableList.remove(banSelected);
                } else {
                    alertController.alertInfo("Xóa thất bại");
                }
            } catch (RemoteException ex) {
                alertController.alertError(ex.getMessage());
            } finally {
                dialog.close();
            }
        });
        JFXButton btn_Cancel = new JFXButton();
        btn_Cancel.setText("Huỷ");
        btn_Cancel.setOnAction(e -> dialog.close());
        dialogLayout.setActions(btn_Ok, btn_Cancel);

        dialog.show();
    }

    /**
     * Phương thức xử lý khi người dùng nhấn button thêm bàn
     * @param event
     * @throws IOException
     */
    @FXML
    private void onClickThemBan(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/admin/popup/ModifierBanScene.fxml"));
        root = loader.load();
        scene = new Scene(root, 558, 530);


        ModifierBanController modifierBanController = loader.getController();
        modifierBanController.setTableBan(tblBan);

        stage = new Stage();
        stage.setScene(scene);

        stage.initOwner(((Node)(event.getSource())).getScene().getWindow());
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setResizable(false);
        stage.showAndWait();
    }

    /**
     * Phương thức xử lý khi người dùng tìm kiếm
     * @param actionEvent
     */
    @FXML
    public void onTimKiem(ActionEvent actionEvent) {
        try {
            String keyword = txtTimKiem.getText().trim();
            List<Ban> bans = null;

            if (keyword.isEmpty()) {
                bans = banDAO.find();

                if (bans != null && bans.size() > 0) {
                    setBanObservableList(FXCollections.observableList(bans));
                }
            } else {
                bans = banDAO.fullTextSearch(keyword);

                if (bans == null || bans.isEmpty()) {
                    alertController.alertInfo("Không tìm thấy thông tin bàn");
                } else {
                    setBanObservableList(FXCollections.observableList(bans));
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    // endregion
}
