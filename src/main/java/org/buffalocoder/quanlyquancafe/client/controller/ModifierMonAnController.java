package org.buffalocoder.quanlyquancafe.client.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.buffalocoder.quanlyquancafe.common.entities.DanhMucMonAn;
import org.buffalocoder.quanlyquancafe.common.entities.MonAn;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.interfaces.IDanhMucMonAnDAO;
import org.buffalocoder.quanlyquancafe.common.interfaces.IMonAnDAO;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.common.utils.Validate;

import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.ResourceBundle;

public class ModifierMonAnController implements Initializable {
    private double x, y;
    private TableView<MonAn> tblMonAn;
    private AlertController alertController;
    private MonAn monAnSelected;
    private IMonAnDAO  monAnDAO;
    private IDanhMucMonAnDAO danhMucMonAnDAO;
    private boolean isEdit;
    private List<DanhMucMonAn> danhMucMonAns;

    @FXML private StackPane stackPane;
    @FXML private JFXTextField txtMaMonAn, txtTenMonAn, txtGiaBan, txtMoTa;
    @FXML private Label lblTitle;
    @FXML private JFXButton btnSave;
    @FXML private FontAwesomeIcon iconSave;
    @FXML private JFXComboBox<String> comboBoxDanhMuc;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);
        isEdit = false;

        try {
            monAnDAO = Services.getInstance().getMonAnService();
            danhMucMonAnDAO = Services.getInstance().getDanhMucMonAnService();

            danhMucMonAns = danhMucMonAnDAO.find();
            danhMucMonAns.forEach(danhMucMonAn -> comboBoxDanhMuc.getItems().add(danhMucMonAn.getTenDanhMuc()));
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    // region Methods
    /**
     * Phương thức truyền vào bảng món ăn từ scene cha
     * @param tblMonAn
     */
    public void setTableMonAn(TableView<MonAn> tblMonAn) {
        this.tblMonAn = tblMonAn;
        try {
            txtMaMonAn.setText(monAnDAO.generateId());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức truyền vào món ăn được chọn từ scene cha
     * @param monAnSelected
     */
    public void setMonAnSelected(MonAn monAnSelected) {
        this.monAnSelected = monAnSelected;
        this.isEdit = true;
        txtMaMonAn.setText(monAnSelected.getMaMonAn());
        txtTenMonAn.setText(monAnSelected.getTenMonAn());
        txtGiaBan.setText(String.valueOf(monAnSelected.getGiaBan()));
        txtMoTa.setText(monAnSelected.getMoTa());

        if (monAnSelected.getDanhMucMonAn() != null) {
            comboBoxDanhMuc.setValue(monAnSelected.getDanhMucMonAn().getTenDanhMuc());
        }

        lblTitle.setText("Cập nhật món ăn");
        btnSave.setText("Lưu");
        iconSave.setGlyphName("EDIT");
    }

    private DanhMucMonAn getDanhMucMonAnSelected() {
        DanhMucMonAn danhMucMonAnSelected = null;

        try {
            int index = comboBoxDanhMuc.getSelectionModel().getSelectedIndex();

            if (index == -1) {
                alertController.alertError("Vui lòng chọn danh mục cho món ăn");
                return null;
            }

            danhMucMonAnSelected = danhMucMonAns.get(index);
        } catch (Exception e) {
            alertController.alertError(e.getMessage());
        }

        return danhMucMonAnSelected;
    }

    /**
     * Phương thức kiểm tra dữ liệu
     * @return
     */
    private boolean validate() {
        if (!Validate.isNumber(txtGiaBan.getText().trim())) {
            alertController.alertError("Vui lòng nhập giá bán là số");
            return false;
        }

        return true;
    }

    /**
     * Phương thức xử lý thêm món ăn
     * @param actionEvent
     * @param monAn
     */
    private void themMonAn(MonAn monAn, ActionEvent actionEvent) throws RemoteException {
        if (monAnDAO.add(monAn)) {

            if (tblMonAn != null) {
                tblMonAn.getItems().add(monAn);
                onClose(actionEvent);
                alertController.alertInfo("Thêm món ăn thành công");
            }
        } else alertController.alertError("Không thể thêm món ăn vào lúc này");
    }

    /**
     * Phương thức xử lý cập nhật món ăn
     * @param actionEvent
     * @param monAn
     */
    private void capNhatMonAn(MonAn monAn, ActionEvent actionEvent) throws RemoteException {
        if (monAnDAO.update(monAn)) {
            if (tblMonAn != null) {
                onClose(actionEvent);
                int index = tblMonAn.getItems().indexOf(monAnSelected);
                tblMonAn.getItems().set(index, monAn);
                alertController.alertInfo("Cập nhật thông tin món ăn thành công");
            }
        } else alertController.alertError("Không thể cập nhật thông tin món ăn vào lúc này");
    }
    // endregion

    // region Events
    /**
     * Phương thức đóng scene
     *
     * @param actionEvent
     */
    @FXML
    private void onClose(ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.close();
    }

    /**
     * Phương thức kéo thả scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onDragged(MouseEvent mouseEvent) {
        Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        stage.setX(mouseEvent.getScreenX() - x);
        stage.setY(mouseEvent.getScreenY() - y);
    }

    /**
     * Phương thức xử lý khi nhấn vào scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onPressed(MouseEvent mouseEvent) {
        x = mouseEvent.getSceneX();
        y = mouseEvent.getSceneY();
    }

    /**
     * Phương thức xử lý khi người dùng nhấn nút lưu
     * @param actionEvent
     */
    @FXML
    public void onClickSave(ActionEvent actionEvent) {
        if (!validate()) return;

        try {
            MonAn monAn = new MonAn (
                    txtMaMonAn.getText().trim(),
                    txtTenMonAn.getText().trim(),
                    Double.parseDouble(txtGiaBan.getText().trim()),
                    txtMoTa.getText().trim(),
                    getDanhMucMonAnSelected()
            );

            if (isEdit) {
                capNhatMonAn(monAn, actionEvent);
            } else {
                themMonAn(monAn, actionEvent);
            }
        } catch (ValidateException | RemoteException e) {
            alertController.alertError(e.getMessage());
        }
    }
    // endregion
}
