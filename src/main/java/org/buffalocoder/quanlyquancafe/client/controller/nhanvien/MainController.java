package org.buffalocoder.quanlyquancafe.client.controller.nhanvien;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.buffalocoder.quanlyquancafe.client.controller.AlertController;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    private double x, y;
    private AlertController alertController;
    private Parent root;

    @FXML
    private Pane pane_ThongKe;
    @FXML
    private StackPane stackPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        alertController = new AlertController(stackPane);

        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/nhanvien/HomeScene.fxml"));
            root.getStylesheets().add(getClass().getResource("/css/common.css").toExternalForm());
        } catch (IOException e) {
            e.printStackTrace();
        }
        pane_ThongKe.getChildren().removeAll();
        pane_ThongKe.getChildren().setAll(root);
    }


    // region Event
    /**
     * Phương thức đóng scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onClose(MouseEvent mouseEvent) {
        alertController.alertYesNo("Đóng ứng dụng", "Bạn có muốn đóng ứng dụng?");
    }

    /**
     * Phương thức kéo thả scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onDragged(MouseEvent mouseEvent) {
        Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        stage.setX(mouseEvent.getScreenX() - x);
        stage.setY(mouseEvent.getScreenY() - y);
    }

    /**
     * Phương thức xử lý khi nhấn vào scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onPressed(MouseEvent mouseEvent) {
        x = mouseEvent.getSceneX();
        y = mouseEvent.getSceneY();
    }

    /**
     * Phương thức xử lý thu nhỏ scene
     *
     * @param event
     */
    @FXML
    void onMinimizeWindow(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }
    // endregion
}
