package org.buffalocoder.quanlyquancafe.client.controller.nhanvien;

import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.buffalocoder.quanlyquancafe.client.controller.AlertController;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.common.entities.Ban;
import org.buffalocoder.quanlyquancafe.common.entities.HoaDon;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.interfaces.IBanDAO;
import org.buffalocoder.quanlyquancafe.common.interfaces.IHoaDonDAO;

import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

public class ThanhToanController implements Initializable {
    public JFXTextField txtGiamGia;
    private double x, y;
    private IHoaDonDAO hoaDonDAO;
    private IBanDAO banDAO;
    private AlertController alertController;
    private HoaDon hoaDon;
    private GridPane gridPaneDatMon;
    private Pane pane;

    @FXML private StackPane stackPane;
    @FXML private Label lblTongTien, lblTienThua, lblThanhTien;
    @FXML private JFXTextField txtTienKhachTra;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);

        try {
            hoaDonDAO = Services.getInstance().getHoaDonService();
            banDAO = Services.getInstance().getBanService();
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    // region Events
    /**
     * Phương thức đóng scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onClose(MouseEvent mouseEvent) {
        Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        stage.close();
    }

    /**
     * Phương thức kéo thả scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onDragged(MouseEvent mouseEvent) {
        Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        stage.setX(mouseEvent.getScreenX() - x);
        stage.setY(mouseEvent.getScreenY() - y);
    }

    /**
     * Phương thức xử lý khi nhấn vào scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onPressed(MouseEvent mouseEvent) {
        x = mouseEvent.getSceneX();
        y = mouseEvent.getSceneY();
    }

    @FXML
    public void onTextChangeTienKhachTra(KeyEvent keyEvent) {
        try {
            if (!txtTienKhachTra.getText().trim().isEmpty()) {
                hoaDon.setTienKhachTra(Double.parseDouble(txtTienKhachTra.getText().trim()));
            }
            lblTienThua.setText(String.valueOf(hoaDon.getTienThua()));
        } catch (ValidateException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onTextChangeGiamGia(KeyEvent keyEvent) {
        try {
            if (!txtGiamGia.getText().trim().isEmpty()) {
                hoaDon.setGiamGia(Double.parseDouble(txtGiamGia.getText().trim()));
            }
            lblThanhTien.setText(String.valueOf(hoaDon.thanhTien()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onThanhToan(ActionEvent actionEvent) throws RemoteException {
        System.out.println(hoaDon.getGiamGia());
        if (hoaDon.getTienKhachTra() >= (hoaDon.getTongTien() - hoaDon.getGiamGia())) {
            // xóa hết danh sách trong list order
            Set<Node> deleteNodes = new HashSet<>(gridPaneDatMon.getChildren());
            gridPaneDatMon.getChildren().removeAll(deleteNodes);

            hoaDonDAO.update(hoaDon);

            hoaDon.getBan().setTrangThai(false);
            banDAO.update(hoaDon.getBan());

            alertController.alertInfo("Thanh toán thành công");

            // dong dialog
            Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            stage.close();
        }
    }
    // endregion

    // region Methods
    public void setPane(Pane pane) {
        this.pane = pane;
    }

    public void setHoaDon(HoaDon hoaDon) {
        this.hoaDon = hoaDon;

        try {
            lblTongTien.setText(String.valueOf(hoaDon.getTongTien()));
            lblTienThua.setText(String.valueOf(hoaDon.getTienThua()));
            lblThanhTien.setText(String.valueOf(hoaDon.thanhTien()));
        } catch (Exception e) {}
    }

    public void setGridPaneDatMon(GridPane gridPaneDatMon) {
        this.gridPaneDatMon = gridPaneDatMon;
    }
    // endregion
}
