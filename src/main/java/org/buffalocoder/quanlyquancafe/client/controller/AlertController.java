package org.buffalocoder.quanlyquancafe.client.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class: AlertController
 *
 * @author: Đặng Lê Minh Trường, Nguyễn Chí Cường
 * @version: 1.1
 * @description: Tạo các dialog thông báo lỗi, thoát, validation...
 * @since : 05-10-2019
 */
public class AlertController {
    private StackPane stackPane;

    public AlertController(StackPane stackPane) {
        this.stackPane = stackPane;
    }

    public void alertYesNo(String title, String content) {

        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text(title));
        dialogLayout.setBody(new Text(content));
        JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btn_Ok = new JFXButton();
        btn_Ok.setText("Đồng ý");
        btn_Ok.setOnAction(event -> {
            System.exit(0);
        });
        JFXButton btn_Cancel = new JFXButton();
        btn_Cancel.setText("Huỷ");
        btn_Cancel.setOnAction(e -> dialog.close());
        dialogLayout.setActions(btn_Ok, btn_Cancel);

        dialog.show();
    }

    public boolean alertValidation(String title, String content) {

        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text(title));
        dialogLayout.setBody(new Text(content));
        JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btn_Cancel = new JFXButton();
        btn_Cancel.setText("Đóng");
        btn_Cancel.setOnAction(e -> {
            dialog.close();
        });
        dialogLayout.setActions(btn_Cancel);

        dialog.show();
        return true;
    }

    public void alertExitCRUD(String title, String content, JFXButton btn_Huy) {
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text(title));
        dialogLayout.setBody(new Text(content));
        JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btn_Ok = new JFXButton();
        btn_Ok.setText("Đồng ý");
        btn_Ok.setOnAction(event -> {
            Stage stage = (Stage) btn_Huy.getScene().getWindow();
            stage.close();
        });
        JFXButton btn_Cancel = new JFXButton();
        btn_Cancel.setText("Huỷ");
        btn_Cancel.setOnAction(e -> {
            dialog.close();
        });
        dialogLayout.setActions(btn_Ok, btn_Cancel);
        dialog.show();
    }

    public boolean alertError(String content) {
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Lỗi"));
        dialogLayout.setBody(new Text(content));
        JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btn_Cancel = new JFXButton();
        btn_Cancel.setText("Đóng");
        btn_Cancel.setOnAction(e -> {
            dialog.close();
        });
        dialogLayout.setActions(btn_Cancel);

        dialog.show();
        return true;
    }

    public boolean alertWarning(String content) {
        AtomicBoolean result = new AtomicBoolean(false);

        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Cảnh báo"));
        dialogLayout.setBody(new Text(content));
        JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btn_Ok = new JFXButton();
        btn_Ok.setText("Đồng ý");
        btn_Ok.setOnAction(event -> {
            result.set(true);
            return;
        });
        JFXButton btn_Cancel = new JFXButton();
        btn_Cancel.setText("Huỷ");
        btn_Cancel.setOnAction(e -> dialog.close());
        dialogLayout.setActions(btn_Ok, btn_Cancel);

        dialog.show();

        return result.get();
    }

    public void alertInfo(String content) {
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Thông báo"));
        dialogLayout.setBody(new Text(content));
        JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btnOK = new JFXButton();
        btnOK.setText("Đồng ý");
        btnOK.setOnAction(event -> dialog.close());
        dialogLayout.setActions(btnOK);

        dialog.show();
    }

}
