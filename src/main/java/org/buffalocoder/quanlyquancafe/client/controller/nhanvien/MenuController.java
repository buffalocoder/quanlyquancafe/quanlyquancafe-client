package org.buffalocoder.quanlyquancafe.client.controller.nhanvien;

import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Initializable {
    @FXML
    private ListView<String> MonanList;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        MonanList.setItems( FXCollections.observableArrayList("Nước uống", "Món ăn"));
    }
}
