package org.buffalocoder.quanlyquancafe.client.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.common.entities.Ban;
import org.buffalocoder.quanlyquancafe.common.entities.DanhMucMonAn;
import org.buffalocoder.quanlyquancafe.common.entities.MonAn;
import org.buffalocoder.quanlyquancafe.common.interfaces.IDanhMucMonAnDAO;
import org.buffalocoder.quanlyquancafe.common.interfaces.IMonAnDAO;

import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.ResourceBundle;

public class QuanLyDanhMucController implements Initializable {
    public JFXButton btn_Them;
    private Scene scene;
    private Stage stage;
    private Parent root;
    private AlertController alertController;
    private ObservableList<DanhMucMonAn> danhMucMonAnObservableList;
    private IDanhMucMonAnDAO danhMucMonAnDAO;
    private IMonAnDAO monAnDAO;

    @FXML private StackPane stackPane;
    @FXML private TextField txtTimKiem;
    @FXML private TableView<DanhMucMonAn> tblDanhMuc;
    @FXML private TableColumn<DanhMucMonAn, String> colMaDanhMuc;
    @FXML private TableColumn<DanhMucMonAn, String> colTenDanhMuc;
    @FXML private TableColumn<DanhMucMonAn, Integer> colSoLuongMonAn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);

        initTable();

        try {
            danhMucMonAnDAO = Services.getInstance().getDanhMucMonAnService();
            monAnDAO = Services.getInstance().getMonAnService();
            List<DanhMucMonAn> danhMucMonAns = danhMucMonAnDAO.find();

            if (danhMucMonAns != null && danhMucMonAns.size() > 0) {
                setDanhMucMonAnObservableList(FXCollections.observableList(danhMucMonAns));
            }
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    // region Methods
    /**
     * Phương thức khởi tạo table
     */
    private void initTable() {
        colMaDanhMuc.setCellValueFactory(new PropertyValueFactory<DanhMucMonAn, String>("maDanhMuc"));
        colTenDanhMuc.setCellValueFactory(new PropertyValueFactory<DanhMucMonAn, String>("tenDanhMuc"));
        colSoLuongMonAn.setCellValueFactory(cellData ->
                new SimpleObjectProperty<>(getSoLuongMonAn(cellData.getValue().getMaDanhMuc())));
    }

    private int getSoLuongMonAn(String maDanhMuc) {
        int soLuong = 0;

        try {
            soLuong = monAnDAO.findByMaDanhMuc(maDanhMuc).size();
        } catch (Exception e) {
            return 0;
        }

        return soLuong;
    }

    public void setDanhMucMonAnObservableList(ObservableList<DanhMucMonAn> danhMucMonAnObservableList) {
        this.danhMucMonAnObservableList = danhMucMonAnObservableList;
        tblDanhMuc.setItems(danhMucMonAnObservableList);
    }

    /**
     * Phương thức lấy item đang được chọn
     * @return DanhMucMonAn
     */
    private DanhMucMonAn getItemSelected() {
        DanhMucMonAn danhMucMonAnSelected = null;

        try {
            danhMucMonAnSelected = tblDanhMuc.getSelectionModel().getSelectedItem();
        } catch (Exception e) {
            alertController.alertError("Vui lòng chọn danh mục món ăn");
        }

        if (danhMucMonAnSelected == null) {
            alertController.alertError("Vui lòng chọn danh mục món ăn");

        }

        return danhMucMonAnSelected;
    }
    // endregion

    // region Events

    /**
     * Phương thức xử lý khi người dùng nhấn button thêm danh mục
     * @param actionEvent
     */
    @FXML
    private void onClickThemDanhMucMonAn(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/admin/popup/ModifierDanhMucScene.fxml"));
            root = loader.load();
            scene = new Scene(root, 558, 417);

            ModifierDanhMucController modifierDanhMucController = loader.getController();
            modifierDanhMucController.setTableDanhMucMonAn(tblDanhMuc);

            stage = new Stage();
            stage.initOwner(((Node)(actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức xử lý khi người dùng nhấn button sửa món ăn
     * @param actionEvent
     */
    @FXML
    private void onClickCapNhatDanhMucMonAn(ActionEvent actionEvent) {
        DanhMucMonAn danhMucMonAnSelected = getItemSelected();
        if (danhMucMonAnSelected == null) return;

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/admin/popup/ModifierDanhMucScene.fxml"));
            root = loader.load();
            scene = new Scene(root, 558, 417);

            ModifierDanhMucController modifierDanhMucController = loader.getController();
            modifierDanhMucController.setTableDanhMucMonAn(tblDanhMuc);
            modifierDanhMucController.setDanhMucMonAnSelected(danhMucMonAnSelected);

            stage = new Stage();
            stage.initOwner(((Node)(actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức xử lý khi người dùng nhấn button xóa danh mục
     * @param actionEvent
     */
    @FXML
    private void onClickXoaDanhMucMonAn(ActionEvent actionEvent) {
        DanhMucMonAn danhMucMonAnSelected = getItemSelected();

        if (danhMucMonAnSelected == null) return;

        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Cảnh báo"));
        dialogLayout.setBody(new Text(
                String.format("Bạn có chắc muốn xoá danh mục %s không ?\nThao tác này sẽ xóa tất cả món ăn có trong danh mục",
                        danhMucMonAnSelected.getTenDanhMuc())));
        JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btnOK = new JFXButton();
        btnOK.setText("Đồng ý");
        btnOK.setOnAction(e -> {
            try {
                if (danhMucMonAnDAO.deleteById(danhMucMonAnSelected.getMaDanhMuc())) {
                    danhMucMonAnObservableList.remove(danhMucMonAnSelected);
                } else {
                    alertController.alertInfo(String.format("Không thể xóa danh mục %s lúc này", danhMucMonAnSelected.getTenDanhMuc()));
                }
            } catch (RemoteException ex) {
                alertController.alertError(ex.getMessage());
            } finally {
                dialog.close();
            }
        });
        JFXButton btnCancel = new JFXButton();
        btnCancel.setText("Huỷ");
        btnCancel.setOnAction(e -> dialog.close());
        dialogLayout.setActions(btnOK, btnCancel);

        dialog.show();
    }

    /**
     * Phương thức xử lý khi người dùng tìm kiếm
     * @param actionEvent
     */
    @FXML
    public void onTimKiem(ActionEvent actionEvent) {
        try {
            String keyword = txtTimKiem.getText().trim();
            List<DanhMucMonAn> danhMucMonAns = null;

            if (keyword.isEmpty()) {
                danhMucMonAns = danhMucMonAnDAO.find();

                if (danhMucMonAns != null && danhMucMonAns.size() > 0) {
                    setDanhMucMonAnObservableList(FXCollections.observableList(danhMucMonAns));
                }
            } else {
                danhMucMonAns = danhMucMonAnDAO.fullTextSearch(keyword);

                if (danhMucMonAns == null || danhMucMonAns.isEmpty()) {
                    alertController.alertInfo("Không tìm thấy thông tin danh mục món ăn");
                } else {
                    setDanhMucMonAnObservableList(FXCollections.observableList(danhMucMonAns));
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    // endregion
}
