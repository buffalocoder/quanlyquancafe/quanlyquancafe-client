package org.buffalocoder.quanlyquancafe.client.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class QuanLyController implements Initializable {
    private double x, y;
    private AlertController alertController;

    @FXML private VBox fragment;
    @FXML private StackPane stackPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);
    }

    // region Events

    /**
     * Phương thức đóng scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onClose(MouseEvent mouseEvent) {
        alertController.alertYesNo("Đóng ứng dụng", "Bạn có muốn đóng ứng dụng?");
    }

    /**
     * Phương thức kéo thả scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onDragged(MouseEvent mouseEvent) {
        Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        stage.setX(mouseEvent.getScreenX() - x);
        stage.setY(mouseEvent.getScreenY() - y);
    }

    /**
     * Phương thức xử lý khi nhấn vào scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onPressed(MouseEvent mouseEvent) {
        x = mouseEvent.getSceneX();
        y = mouseEvent.getSceneY();
    }

    /**
     * Phương thức xử lý thu nhỏ scene
     *
     * @param event
     */
    @FXML
    void onMinimizeWindow(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }

    /**
     * Phương thức xử lý khi nhấn button Quản lý bàn
     *
     * @param event
     * @throws IOException
     */
    @FXML
    private void onClickQuanLyBan(MouseEvent event) throws IOException {
        Parent pane = FXMLLoader.load(getClass().getResource("/fxml/admin/QuanLyBanScene.fxml"));
        fragment.getChildren().setAll(pane);
    }

    /**
     * Phương thức xử lý khi nhấn button Quản lý danh mục
     *
     * @param event
     * @throws IOException
     */
    @FXML
    private void onClickQuanLyDanhMuc(MouseEvent event) throws IOException {
        Parent pane = FXMLLoader.load(getClass().getResource("/fxml/admin/QuanLyDanhMucScene.fxml"));
        fragment.getChildren().setAll(pane);
    }

    /**
     * Phương thức xử lý khi nhấn button Quản lý món ăn
     * @param event
     * @throws IOException
     */
    @FXML
    private void onClickQuanLyMonAn(MouseEvent event) throws IOException {
        Parent pane = FXMLLoader.load(getClass().getResource("/fxml/admin/QuanLyMonAnScene.fxml"));
        fragment.getChildren().setAll(pane);
    }

    /**
     * Phương thức xử lý khi nhấn nút quản lý nhân viên
     * @param event
     * @throws IOException
     */
    @FXML
    private void onClickQuanLyNhanVien(MouseEvent event) throws IOException {
        Parent pane = FXMLLoader.load(getClass().getResource("/fxml/admin/QuanLyNhanvienScene.fxml"));
        fragment.getChildren().setAll(pane);
    }

    /**
     * Phương thức xử lý khi nhấn button trở về màn hình order
     * @param event
     * @throws IOException
     */
    @FXML
    private void onClickTroVeManHinhBanHang(MouseEvent event) throws IOException {
        try {
            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            stage.close();

            Parent root = FXMLLoader.load(getClass().getResource("/fxml/nhanvien/MainScene.fxml"));
            root.getStylesheets().add(getClass().getResource("/css/common.css").toExternalForm());
            Scene scene = new Scene(root, 1500, 840);
            stage = new Stage();
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức xử lý khi nhấn nút quản lý hóa đơn
     * @param mouseEvent
     * @throws IOException
     */
    @FXML
    public void onClickQuanLyHoaDon(MouseEvent mouseEvent) throws IOException {
        Parent pane = FXMLLoader.load(getClass().getResource("/fxml/admin/QuanLyHoaDonScene.fxml"));
        fragment.getChildren().setAll(pane);
    }
    // endregion
}
