package org.buffalocoder.quanlyquancafe.client.controller;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.common.consts.FieldNameConst;
import org.buffalocoder.quanlyquancafe.common.entities.*;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.interfaces.IHoaDonDAO;
import org.buffalocoder.quanlyquancafe.common.types.GioiTinhEnum;
import org.buffalocoder.quanlyquancafe.common.types.LoaiDienThoaiEnum;
import org.buffalocoder.quanlyquancafe.common.types.LoaiNhanVienEnum;

import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ResourceBundle;

public class QuanLyHoaDonController implements Initializable {
    private AlertController alertController;
    private IHoaDonDAO hoaDonDAO;
    private ObservableList<HoaDon> hoaDonObservableList;

    @FXML private StackPane stackPane;
    @FXML private TableView tblHoaDon;
    @FXML private TableColumn<HoaDon, String> colMaHoaDon;
    @FXML private TableColumn<HoaDon, String> colMaBan;
    @FXML private TableColumn<HoaDon, String> colTenNhanVien;
    @FXML private TableColumn<HoaDon, Double> colTongTien;
    @FXML private TableColumn<HoaDon, LocalDate> colNgayXuatHoaDon;
    @FXML private TableColumn<HoaDon, String> colGhiChu;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);

        initTable();

        try {
            hoaDonDAO = Services.getInstance().getHoaDonService();
            List<HoaDon> hoaDons = hoaDonDAO.find();
            tblHoaDon.setItems(FXCollections.observableList(hoaDons));
        } catch (RemoteException | NotBoundException e) {
            alertController.alertError(e.getMessage());
        }
    }

    // region Methods
    private void initTable() {
        colMaHoaDon.setCellValueFactory(new PropertyValueFactory<HoaDon, String>(FieldNameConst.HOADON_MAHOADON));
        colMaBan.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getBan().getMaBan()));
        colTenNhanVien.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getNhanVien().getTenNhanVien()));
        colTongTien.setCellValueFactory(cellData ->
                new SimpleObjectProperty<>(cellData.getValue().getTongTien()));
        colNgayXuatHoaDon.setCellValueFactory(new PropertyValueFactory<HoaDon, LocalDate>(FieldNameConst.HOADON_NGAYXUATHOADON));
        colGhiChu.setCellValueFactory(new PropertyValueFactory<HoaDon, String>(FieldNameConst.HOADON_GHICHU));
    }
    // endregion

    // region Events
    // endregion
}
