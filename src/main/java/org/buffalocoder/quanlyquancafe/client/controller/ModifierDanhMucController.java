package org.buffalocoder.quanlyquancafe.client.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.buffalocoder.quanlyquancafe.common.entities.DanhMucMonAn;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.interfaces.IDanhMucMonAnDAO;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;

import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

public class ModifierDanhMucController implements Initializable {
    private double x, y;
    private TableView<DanhMucMonAn> tblDanhMuc;
    private AlertController alertController;
    private DanhMucMonAn danhMucMonAnSelected;
    private IDanhMucMonAnDAO danhMucMonAnDAO;
    private boolean isEdit;

    @FXML private StackPane stackPane;
    @FXML private JFXTextField txtMaDanhMuc, txtTenDanhMuc;
    @FXML private Label lblTitle;
    @FXML private JFXButton btnSave;
    @FXML private FontAwesomeIcon iconSave;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);
        isEdit = false;

        try {
            danhMucMonAnDAO = Services.getInstance().getDanhMucMonAnService();
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    // region Methods
    /**
     * Phương thức truyền vào bảng danh mục món ăn từ scene cha
     * @param tblDanhMuc
     */
    public void setTableDanhMucMonAn(TableView<DanhMucMonAn> tblDanhMuc) {
        this.tblDanhMuc = tblDanhMuc;
        try {
            txtMaDanhMuc.setText(danhMucMonAnDAO.generateId());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức truyền vào danh mục món ăn được chọn từ scene cha
     * @param danhMucMonAnSelected
     */
    public void setDanhMucMonAnSelected(DanhMucMonAn danhMucMonAnSelected) {
        this.danhMucMonAnSelected = danhMucMonAnSelected;
        this.isEdit = true;
        txtMaDanhMuc.setText(danhMucMonAnSelected.getMaDanhMuc());
        txtTenDanhMuc.setText(danhMucMonAnSelected.getTenDanhMuc());
        lblTitle.setText("Cập nhật danh mục");
        btnSave.setText("Lưu");
        iconSave.setGlyphName("EDIT");
    }

    /**
     * Phương thức xử lý thêm danh mục món ăn
     * @param actionEvent
     * @param danhMucMonAn
     */
    private void themDanhMucMonAn(DanhMucMonAn danhMucMonAn, ActionEvent actionEvent) throws RemoteException {
        if (danhMucMonAnDAO.add(danhMucMonAn)) {
            if (tblDanhMuc != null) {
                tblDanhMuc.getItems().add(danhMucMonAn);
                onClose(actionEvent);
                alertController.alertInfo("Thêm danh mục món ăn thành công");
            }
        } else alertController.alertError("Không thể thêm danh mục món ăn vào lúc này");
    }

    /**
     * Phương thức xử lý cập nhật danh mục món ăn
     * @param actionEvent
     * @param danhMucMonAn
     */
    private void capNhatDanhMucMonAn(DanhMucMonAn danhMucMonAn, ActionEvent actionEvent) throws RemoteException {
        if (danhMucMonAnDAO.update(danhMucMonAn)) {
            if (tblDanhMuc != null) {
                onClose(actionEvent);
                int index = tblDanhMuc.getItems().indexOf(danhMucMonAnSelected);
                tblDanhMuc.getItems().set(index, danhMucMonAn);
                alertController.alertInfo("Cập nhật thông tin danh mục món ăn thành công");
            }
        } else alertController.alertError("Không thể cập nhật thông tin danh mục món ăn vào lúc này");
    }
    // endregion

    // region Events
    /**
     * Phương thức đóng scene
     *
     * @param actionEvent
     */
    @FXML
    private void onClose(ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.close();
    }

    /**
     * Phương thức kéo thả scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onDragged(MouseEvent mouseEvent) {
        Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        stage.setX(mouseEvent.getScreenX() - x);
        stage.setY(mouseEvent.getScreenY() - y);
    }

    /**
     * Phương thức xử lý khi nhấn vào scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onPressed(MouseEvent mouseEvent) {
        x = mouseEvent.getSceneX();
        y = mouseEvent.getSceneY();
    }

    /**
     * Phương thức xử lý khi người dùng nhấn nút lưu
     * @param actionEvent
     */
    @FXML
    public void onClickSave(ActionEvent actionEvent) {
        try {
            DanhMucMonAn danhMucMonAn = new DanhMucMonAn (
                    txtMaDanhMuc.getText().trim(),
                    txtTenDanhMuc.getText().trim()
            );

            if (isEdit) {
                capNhatDanhMucMonAn(danhMucMonAn, actionEvent);
            } else {
                themDanhMucMonAn(danhMucMonAn, actionEvent);
            }
        } catch (ValidateException | RemoteException e) {
            alertController.alertError(e.getMessage());
        }
    }
    // endregion
}
