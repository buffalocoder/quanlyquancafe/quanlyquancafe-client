package org.buffalocoder.quanlyquancafe.client.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.buffalocoder.quanlyquancafe.client.controller.nhanvien.SodoMonanController;
import org.buffalocoder.quanlyquancafe.client.ultis.Component;
import org.buffalocoder.quanlyquancafe.common.entities.MonAn;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class DatMonanList implements Initializable {
    private ArrayList<String> list;
    public DatMonanList() {
        list = new ArrayList<String>();
    }
    private Component component;

    private Scene scene;
    private Stage stage;
    private Parent root;

    @FXML
    public GridPane GridPaneDatMon;


    public GridPane getGridPaneDatMon() {
        return GridPaneDatMon;
    }

    @FXML
    private TableView<MonAn> monAnTableView;

    @FXML
    public void ThanhToan(ActionEvent event){

    }
    int index =1;

    @FXML
    private void moThanhToan(MouseEvent event) throws IOException, ValidateException {
//        Parent root = FXMLLoader.load(getClass().getResource("/fxml/nhanvien/popup/ThanhToanScene.fxml"));
//        root.getStylesheets().add(getClass().getResource("/css/common.css").toExternalForm());
//        Scene scene = new Scene(root, 565, 587);
//        Stage stage = new Stage();
//        stage.setScene(scene);
//        stage.initStyle(StageStyle.UNDECORATED);
//        stage.setResizable(false);
//        stage.show();

//        try {
//            FXMLLoader loader = new FXMLLoader();
//            loader.setLocation(getClass().getResource("/fxml/admin/popup/ModifierMonAnScene.fxml"));
//            root = loader.load();
//            scene = new Scene(root, 563, 602);
//
//            ModifierMonAnController modifierMonAnController = loader.getController();
//            modifierMonAnController.setTableMonAn(monAnTableView);
//
//            stage = new Stage();
//            stage.setScene(scene);
//            stage.initStyle(StageStyle.UNDECORATED);
//            stage.setResizable(false);
//            stage.showAndWait();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/nhanvien/SodoMonan.fxml"));
        root = loader.load();
        SodoMonanController sodoMonanController =loader.getController();
        sodoMonanController.setIndex(123);
        System.out.println("chuyển dữ liệu");


//        MonAn monAn = new MonAn("MA0001","456",5,"a",null);
//
//        try {
//            FXMLLoader loader = new FXMLLoader();
//            loader.setLocation(getClass().getResource("/fxml/admin/popup/ModifierMonAnScene.fxml"));
//            root = loader.load();
//            scene = new Scene(root, 563, 602);
//
//            ModifierMonAnController modifierMonAnController = loader.getController();
//
//            modifierMonAnController.setMonAnSelected(monAn);
//
//            stage = new Stage();
//            stage.setScene(scene);
//            stage.initStyle(StageStyle.UNDECORATED);
//            stage.setResizable(false);
//            stage.showAndWait();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }





        Pane pane = component.getPaneDatMon("123","24",1,1,1);
        GridPaneDatMon.add(pane,0,index);
        index++;

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
//        System.out.println("trước khi chuyển");
//        component = new Component();
//        //GridPaneDatMon = new GridPane();
//        try {
//
//            Pane pane = component.getPaneDatMon("123","24",1,1,1);
//            GridPaneDatMon.add(pane,0,0);
//            //GridPaneDatMon.add(pane,0,1);
//            FXMLLoader loader = new FXMLLoader();
//            loader.setLocation(getClass().getResource("/fxml/nhanvien/SodoMonan.fxml"));
//            root = loader.load();
//            if(GridPaneDatMon!=null){
//                System.out.println("gridpane ko null");
//            }
//
//            SodoMonanController sodoMonanController =loader.getController();
//            sodoMonanController.setGridPaneDatMon(GridPaneDatMon);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("sau khi chuyển");


    }
}
