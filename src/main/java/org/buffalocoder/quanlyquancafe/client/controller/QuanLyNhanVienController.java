package org.buffalocoder.quanlyquancafe.client.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.common.consts.FieldNameConst;
import org.buffalocoder.quanlyquancafe.common.entities.NhanVien;
import org.buffalocoder.quanlyquancafe.common.interfaces.INhanVienDAO;
import org.buffalocoder.quanlyquancafe.common.types.GioiTinhEnum;

import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.ResourceBundle;

public class QuanLyNhanVienController implements Initializable {
    private Scene scene;
    private Stage stage;
    private Parent root;
    private AlertController alertController;
    private ObservableList<NhanVien> nhanVienObservableList;
    private INhanVienDAO nhanVienDAO;

    @FXML
    private StackPane stackPane;
    @FXML
    private TextField txtTimKiem;
    @FXML
    private TableView<NhanVien> tblNhanVien;
    @FXML
    private TableColumn<NhanVien, String> colMaNhanVien;
    @FXML
    private TableColumn<NhanVien, String> colHoTen;
    @FXML
    private TableColumn<NhanVien, String> colGioiTinh;
    @FXML
    private TableColumn<NhanVien, String> colEmail;
    @FXML
    private TableColumn<NhanVien, String> colChungMinhNhanDan;
    @FXML
    private TableColumn<NhanVien, String> colSoDienThoai;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);

        initTable();

        try {
            nhanVienDAO = Services.getInstance().getNhanVienService();
            List<NhanVien> nhanViens = nhanVienDAO.find();

            if (nhanViens != null && nhanViens.size() > 0) {
                setNhanVienObservableList(FXCollections.observableList(nhanViens));
            }
        } catch (RemoteException | NotBoundException e) {
            alertController.alertError(e.getMessage());
        }
    }

    // region Methods

    /**
     * Phương thức khởi tạo table
     */
    private void initTable() {
        colMaNhanVien.setCellValueFactory(new PropertyValueFactory<NhanVien, String>(FieldNameConst.NHANVIEN_MANHANVIEN));
        colHoTen.setCellValueFactory(new PropertyValueFactory<NhanVien, String>(FieldNameConst.NHANVIEN_TENNHANVIEN));
        colGioiTinh.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getGioiTinh()));
        colChungMinhNhanDan.setCellValueFactory(new PropertyValueFactory<NhanVien, String>(FieldNameConst.NHANVIEN_CMND));
        colSoDienThoai.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getDienThoai().getSoDienThoai()));
        colEmail.setCellValueFactory(new PropertyValueFactory<NhanVien, String>(FieldNameConst.NHANVIEN_EMAIL));
    }

    public void setNhanVienObservableList(ObservableList<NhanVien> nhanVienObservableList) {
        this.nhanVienObservableList = nhanVienObservableList;
        tblNhanVien.setItems(nhanVienObservableList);
    }

    /**
     * Phương thức lấy item đang được chọn
     *
     * @return NhanVien
     */
    private NhanVien getItemSelected() {
        NhanVien nhanVienSelected = null;

        try {
            nhanVienSelected = tblNhanVien.getSelectionModel().getSelectedItem();
        } catch (Exception e) {
            alertController.alertError("Vui lòng chọn nhân viên");
        }

        if (nhanVienSelected == null) {
            alertController.alertError("Vui lòng chọn nhân viên");
        }

        return nhanVienSelected;
    }
    // endregion

    // region Events

    /**
     * Phương thức xử lý khi người dùng nhấn button thêm nhân viên
     *
     * @param actionEvent
     */
    @FXML
    public void onClickThemNhanVien(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/admin/popup/ModifierNhanVienScene.fxml"));
            root = loader.load();
            scene = new Scene(root, 760, 844);

            ModifierNhanVienController modifierNhanVienController = loader.getController();
            modifierNhanVienController.setTableNhanVien(tblNhanVien);

            stage = new Stage();
            stage.initOwner(((Node)(actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            alertController.alertError(e.getMessage());
        }
    }

    /**
     * Phương thức xử lý khi người dùng nhấn button xóa nhân viên
     *
     * @param actionEvent
     */
    @FXML
    public void onClickXoaNhanVien(ActionEvent actionEvent) {
        NhanVien nhanVienSelected = getItemSelected();

        if (nhanVienSelected == null) return;

        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Cảnh báo"));
        dialogLayout.setBody(new Text(String.format("Bạn có chắc muốn xoá nhân viên %s không?", nhanVienSelected.getTenNhanVien())));
        JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btn_Ok = new JFXButton();
        btn_Ok.setText("Đồng ý");
        btn_Ok.setOnAction(e -> {
            try {
                if (nhanVienDAO.deleteById(nhanVienSelected.getMaNhanVien())) {
                    nhanVienObservableList.remove(nhanVienSelected);
                } else {
                    alertController.alertInfo(String.format("Không thể xóa nhân viên %s lúc này", nhanVienSelected.getMaNhanVien()));
                }
            } catch (RemoteException ex) {
                alertController.alertError(ex.getMessage());
            } finally {
                dialog.close();
            }
        });
        JFXButton btn_Cancel = new JFXButton();
        btn_Cancel.setText("Huỷ");
        btn_Cancel.setOnAction(e -> dialog.close());
        dialogLayout.setActions(btn_Ok, btn_Cancel);

        dialog.show();
    }

    /**
     * Phương thức xử lý khi người dùng nhấn button cập nhật thông tin nhân viên
     *
     * @param actionEvent
     */
    @FXML
    public void onClickCapNhatNhanVien(ActionEvent actionEvent) {
        NhanVien nhanVienSelected = getItemSelected();

        if (nhanVienSelected == null) return;

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/admin/popup/ModifierNhanVienScene.fxml"));
            root = loader.load();
            scene = new Scene(root, 760, 844);

            ModifierNhanVienController modifierNhanVienController = loader.getController();
            modifierNhanVienController.setTableNhanVien(tblNhanVien);
            modifierNhanVienController.setNhanVienSelected(nhanVienSelected);

            stage = new Stage();
            stage.initOwner(((Node)(actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            alertController.alertError(e.getMessage());
        }
    }

    /**
     * Phương thức xử lý khi người dùng tìm kiếm
     *
     * @param actionEvent
     */
    @FXML
    public void onTimKiem(ActionEvent actionEvent) {
        try {
            String keyword = txtTimKiem.getText().trim();
            List<NhanVien> nhanViens = null;

            if (keyword.isEmpty()) {
                nhanViens = nhanVienDAO.find();

                if (nhanViens != null && nhanViens.size() > 0) {
                    setNhanVienObservableList(FXCollections.observableList(nhanViens));
                }
            } else {
                nhanViens = nhanVienDAO.fullTextSearch(keyword);

                if (nhanViens == null || nhanViens.isEmpty()) {
                    alertController.alertInfo("Không tìm thấy thông tin nhân viên");
                } else {
                    setNhanVienObservableList(FXCollections.observableList(nhanViens));
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    // endregion
}
