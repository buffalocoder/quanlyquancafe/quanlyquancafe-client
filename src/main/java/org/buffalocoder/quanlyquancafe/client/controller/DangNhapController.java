package org.buffalocoder.quanlyquancafe.client.controller;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.client.ultis.StorageAccount;
import org.buffalocoder.quanlyquancafe.common.entities.NhanVien;
import org.buffalocoder.quanlyquancafe.common.interfaces.INhanVienDAO;

import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

public class DangNhapController implements Initializable {
    private double x, y;
    private AlertController alertController;
    private INhanVienDAO nhanVienDAO;

    @FXML
    private JFXTextField txtTaiKhoan;
    @FXML
    private JFXPasswordField txtMatKhau;
    @FXML
    private StackPane stackPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            nhanVienDAO = Services.getInstance().getNhanVienService();
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
        alertController = new AlertController(stackPane);
    }

    // region Event

    /**
     * Phương thức đóng scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onClose(MouseEvent mouseEvent) {
        alertController.alertYesNo("Đóng ứng dụng", "Bạn có muốn đóng ứng dụng?");
    }

    /**
     * Phương thức kéo thả scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onDragged(MouseEvent mouseEvent) {
        Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        stage.setX(mouseEvent.getScreenX() - x);
        stage.setY(mouseEvent.getScreenY() - y);
    }

    /**
     * Phương thức xử lý khi nhấn vào scene
     *
     * @param mouseEvent
     */
    @FXML
    private void onPressed(MouseEvent mouseEvent) {
        x = mouseEvent.getSceneX();
        y = mouseEvent.getSceneY();
    }

    /**
     * Phương thức xử lý thu nhỏ scene
     *
     * @param event
     */
    @FXML
    void onMinimizeWindow(MouseEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }

    /**
     * Phương thức xử lý đăng nhập
     *
     * @param event
     */
    @FXML
    private void onDangNhap(ActionEvent event) {
        String taiKhoan = txtTaiKhoan.getText().trim();
        String matKhau = txtMatKhau.getText().trim();

        if (taiKhoan.isEmpty()) {
            alertController.alertError("Vui lòng nhập tài khoản");
        } else if (matKhau.isEmpty()) {
            alertController.alertError("Vui lòng nhập mật khẩu");
        } else {
            NhanVien nhanVien = null;
            try {
                nhanVien = nhanVienDAO.dangNhap(taiKhoan, matKhau);


                if (nhanVien != null) {
                    StorageAccount.getInstance().setNhanVien(nhanVien);

                    Node node = (Node) event.getSource();
                    Stage stage = (Stage) node.getScene().getWindow();
                    stage.close();
                    openMainScene(stage);
                }
            } catch (Exception e) {
                alertController.alertError(e.getMessage());
            }
        }
    }
    // endregion

    // region Methods

    /**
     * Phương thức xử lý mở màn hình chính
     *
     * @param stage
     * @throws IOException
     */
    private void openMainScene(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/nhanvien/MainScene.fxml"));
        root.getStylesheets().add(getClass().getResource("/css/common.css").toExternalForm());
        stage.setScene(new Scene(root, 1500, 840));
        stage.show();
    }
    // endregion
}
