package org.buffalocoder.quanlyquancafe.client.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.common.entities.MonAn;
import org.buffalocoder.quanlyquancafe.common.interfaces.IMonAnDAO;

import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.ResourceBundle;

//import org.buffalocoder.quanlyquancafe.server.entities.MonAn;
//import org.buffalocoder.quanlyquancafeclinet.connector.ConnectServer;

public class QuanLyMonAnController implements Initializable {
    private Scene scene;
    private Stage stage;
    private Parent root;
    private AlertController alertController;
    private ObservableList<MonAn> monAnObservableList;
    private IMonAnDAO monAnDAO;
    private GridPane gridPane;

    public void setGridPane(GridPane gridPane) {
        this.gridPane = gridPane;
    }

    @FXML
    private StackPane stackPane;
    @FXML
    private TextField txtTimKiem;
    @FXML
    private TableView<MonAn> tblMonAn;
    @FXML
    private TableColumn<MonAn, String> colId;
    @FXML
    private TableColumn<MonAn, String> colTenMonAn;
    @FXML
    private TableColumn<MonAn, Double> colGiaBan;
    @FXML
    private TableColumn<MonAn, String> colMoTa;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        alertController = new AlertController(stackPane);

        initTable();

        try {
            monAnDAO = Services.getInstance().getMonAnService();
            List<MonAn> monAns = monAnDAO.find();

            if (monAns != null && monAns.size() > 0) {
                setMonAnObservableList(FXCollections.observableList(monAns));
            }
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    // region Events

    /**
     * Phương thức xử lý khi người dùng nhấn button thêm món ăn
     *
     * @param actionEvent
     */
    @FXML
    private void onClickThemMonAn(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/admin/popup/ModifierMonAnScene.fxml"));
            root = loader.load();
            scene = new Scene(root, 563, 602);

            ModifierMonAnController modifierMonAnController = loader.getController();
            modifierMonAnController.setTableMonAn(tblMonAn);

            stage = new Stage();
            stage.initOwner(((Node)(actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức xử lý khi người dùng nhấn button sửa món ăn
     *
     * @param actionEvent
     */
    @FXML
    private void onClickCapNhatMonAn(ActionEvent actionEvent) {
        MonAn monAnSelected = getItemSelected();
        if (monAnSelected == null) return;

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/admin/popup/ModifierMonAnScene.fxml"));
            root = loader.load();
            scene = new Scene(root, 563, 602);

            ModifierMonAnController modifierMonAnController = loader.getController();
            modifierMonAnController.setTableMonAn(tblMonAn);
            modifierMonAnController.setMonAnSelected(monAnSelected);

            stage = new Stage();
            stage.setScene(scene);
            stage.initOwner(((Node)(actionEvent.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức xử lý khi người dùng nhấn button xóa món ăn
     *
     * @param actionEvent
     */
    @FXML
    private void onClickXoaMonAn(ActionEvent actionEvent) {
        MonAn monAnSelected = getItemSelected();

        if (monAnSelected == null) return;

        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Cảnh báo"));
        dialogLayout.setBody(new Text(String.format("Bạn có chắc muốn xoá món %s không?", monAnSelected.getTenMonAn())));
        JFXDialog dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton btn_Ok = new JFXButton();
        btn_Ok.setText("Đồng ý");
        btn_Ok.setOnAction(e -> {
            try {
                if (monAnDAO.deleteById(monAnSelected.getMaMonAn())) {
                    monAnObservableList.remove(monAnSelected);
                } else {
                    alertController.alertInfo(String.format("Không thể xóa món %s lúc này", monAnSelected.getTenMonAn()));
                }
            } catch (RemoteException ex) {
                alertController.alertError(ex.getMessage());
            } finally {
                dialog.close();
            }
        });
        JFXButton btn_Cancel = new JFXButton();
        btn_Cancel.setText("Huỷ");
        btn_Cancel.setOnAction(e -> dialog.close());
        dialogLayout.setActions(btn_Ok, btn_Cancel);

        dialog.show();
    }

    /**
     * Phương thức xử lý khi người dùng tìm kiếm
     *
     * @param actionEvent
     */
    @FXML
    public void onTimKiem(ActionEvent actionEvent) {
        try {
            String keyword = txtTimKiem.getText().trim();
            List<MonAn> monAns = null;

            if (keyword.isEmpty()) {
                monAns = monAnDAO.find();

                if (monAns != null && monAns.size() > 0) {
                    setMonAnObservableList(FXCollections.observableList(monAns));
                }
            } else {
                monAns = monAnDAO.fullTextSearch(keyword);

                if (monAns == null || monAns.isEmpty()) {
                    alertController.alertInfo("Không tìm thấy thông tin món ăn");
                } else {
                    setMonAnObservableList(FXCollections.observableList(monAns));
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    // endregion

    // region Methods

    /**
     * Phương thức khởi tạo table
     */
    private void initTable() {
        colId.setCellValueFactory(new PropertyValueFactory<MonAn, String>("maMonAn"));
        colTenMonAn.setCellValueFactory(new PropertyValueFactory<MonAn, String>("tenMonAn"));
        colGiaBan.setCellValueFactory(new PropertyValueFactory<MonAn, Double>("giaBan"));
        colMoTa.setCellValueFactory(new PropertyValueFactory<MonAn, String>("moTa"));
    }

    public void setMonAnObservableList(ObservableList<MonAn> monAnObservableList) {
        this.monAnObservableList = monAnObservableList;
        tblMonAn.setItems(monAnObservableList);
    }

    /**
     * Phương thức lấy item đang được chọn
     *
     * @return MonAn
     */
    private MonAn getItemSelected() {
        MonAn monAnSelected = null;

        try {
            monAnSelected = tblMonAn.getSelectionModel().getSelectedItem();
        } catch (Exception e) {
            alertController.alertError("Vui lòng chọn món ăn");
        }

        if (monAnSelected == null) {
            alertController.alertError("Vui lòng chọn món ăn");
        }

        return monAnSelected;
    }
    // endregion
}
