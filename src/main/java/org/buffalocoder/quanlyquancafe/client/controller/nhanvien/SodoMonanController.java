package org.buffalocoder.quanlyquancafe.client.controller.nhanvien;

import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXScrollPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import org.buffalocoder.quanlyquancafe.client.controller.DatMonanList;
import org.buffalocoder.quanlyquancafe.client.ultis.Component;
import org.buffalocoder.quanlyquancafe.common.entities.Ban;
import org.buffalocoder.quanlyquancafe.common.entities.ChiTietHoaDon;
import org.buffalocoder.quanlyquancafe.common.entities.HoaDon;
import org.buffalocoder.quanlyquancafe.common.entities.MonAn;
import org.buffalocoder.quanlyquancafe.common.exceptions.ValidateException;
import org.buffalocoder.quanlyquancafe.common.interfaces.IHoaDonDAO;
import org.buffalocoder.quanlyquancafe.common.interfaces.IMonAnDAO;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

public class SodoMonanController implements Initializable {
    @FXML
    private JFXListView<Label> danhsachOrder;
    @FXML
    private Pane pane_Monan, pane_OrderBan;

    private List<MonAn> monAnList;

    private Component component;

    private HoaDon hoaDonban;

    private IHoaDonDAO hoaDonDAO;

    @FXML
    public GridPane GridPaneMonAn;


    private GridPane getGridPaneMonAn;


    private Set<ChiTietHoaDon> chiTietHoaDons;

    private Set<ChiTietHoaDon> chiTietHoaDonBan;

    private String MaBan;


    private Set<String> chiTietMaBan;

    private  int Index;

    public void setChiTietMaBan(Set<String> chiTietMaBan) {
        this.chiTietMaBan = chiTietMaBan;
    }

    public void setMaBan(String maBan) {
        MaBan = maBan;
    }

    public void setHoaDonban(HoaDon hoaDonban) {
        this.hoaDonban = hoaDonban;
    }

    public void setChiTietHoaDonBan(Set<ChiTietHoaDon> chiTietHoaDonBan) {
        this.chiTietHoaDonBan = chiTietHoaDonBan;
    }

    public void setChiTietHoaDons(Set<ChiTietHoaDon> chiTietHoaDons) {
        this.chiTietHoaDons = chiTietHoaDons;
    }

    public void setIndex(int index) {
        this.Index = index;
    }

    public void setGridPaneDatMon(GridPane gridPaneDatMon) {
        getGridPaneMonAn = gridPaneDatMon;
    }

    private int index=0;

    private int row,column;

    @FXML
    private void handleButtonMouse(ActionEvent event) throws MalformedURLException {
//        danhsachOrder = new JFXListView<>();
//        danhsachOrder.getItems().add(new Label("Item Cafe" ));
//        danhsachOrder.getStyleClass().add("mylistview");
//        danhsachOrder.setMaxHeight(3400);
//
//
//        StackPane container = new StackPane(danhsachOrder);
//        container.setPadding(new Insets(24));
//
//        JFXScrollPane pane = new JFXScrollPane();
//        pane.setContent(container);
//        pane.setStyle("-fx-background-color: white");
//
//        Label title = new Label("Title");
//        pane.getBottomBar().getChildren().add(title);
//        pane.getBottomBar().setPrefHeight(10);
//        title.setStyle("-fx-text-fill:WHITE; -fx-font-size: 40;");
//        JFXScrollPane.smoothScrolling((ScrollPane) pane.getChildren().get(0));
//
//        StackPane.setMargin(title, new Insets(0, 0, 0, 80));
//        StackPane.setAlignment(title, Pos.CENTER_LEFT);
//
//        pane_OrderBan.getChildren().setAll(pane);

        System.out.println(Index);
        if(getGridPaneMonAn==null){
            System.out.println("bị null");
        }


    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            hoaDonDAO = Services.getInstance().getHoaDonService();
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
        component = new Component();
        try {
            monAnList=loaddulieu();
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }

        chiTietMaBan = new HashSet<>();

        monAnList.forEach(monAn -> {
            try {
                int indexMonAN=index;
                Pane paneMonAN= component.getPaneMonAn(monAn.getTenMonAn(),monAn.getGiaBan());

                paneMonAN.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        //gridPane=new GridPane();
                        component = new Component();
                        int soLuong=1;
                        double thanhTien= monAnList.get(indexMonAN).getGiaBan()*soLuong;
                        System.out.println(indexMonAN);
                        System.out.println(monAnList.get(indexMonAN));
                        try {
                            ChiTietHoaDon chiTietHoaDon = new ChiTietHoaDon(monAnList.get(indexMonAN),1,monAnList.get(indexMonAN).getGiaBan(),thanhTien);
                            //chiTietHoaDons.add(chiTietHoaDon);
                            //chiTietHoaDonBan.add(chiTietHoaDon);
                            hoaDonban.getChiTietHoaDons().add(chiTietHoaDon);
                            ////////////////////////////////
                            hoaDonDAO.update(hoaDonban);
                            ///////////////////////////////

                            System.out.println("đặt món");
                            chiTietHoaDons.forEach(System.out::println);
                            System.out.println("--------------- bàn------------------");
                            chiTietHoaDonBan.forEach(System.out::println);
                            System.out.println("--------------hóa đơn mới-------------------");
                            hoaDonban.getChiTietHoaDons().forEach(System.out::println);
                            System.out.println("---------------------------------");
                           // chiTietHoaDons=hoaDonban.getChiTietHoaDons();
                           // chiTietHoaDonBan=hoaDonban.getChiTietHoaDons();
                            hoaDonban.getChiTietHoaDons().forEach(chiTietHoaDon1 -> {
                                chiTietHoaDons.add(chiTietHoaDon1);
                                chiTietHoaDonBan.add(chiTietHoaDon1);
                            });
                           // MaBan=hoaDonban.getBan().getMaBan();
                            chiTietMaBan.add(hoaDonban.getBan().getMaBan());
                        } catch (ValidateException | RemoteException e) {
                            e.printStackTrace();
                        }

                        //Pane paneDatMon = null;
                        try {
                            Pane paneDatMon = component.getPaneDatMon(monAnList.get(indexMonAN).getMaMonAn(),monAnList.get(indexMonAN).getTenMonAn(),soLuong,monAnList.get(indexMonAN).getGiaBan(),thanhTien);
                            System.out.println(Index);
                            getGridPaneMonAn.add(paneDatMon,0,Index);
                            Index++;
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }



                    }
                });
                GridPaneMonAn.add(paneMonAN,column,row);
                column++;
                if(column==4){
                    row++;
                    column=0;
                }
                index++;
            } catch (Exception e) {
                e.printStackTrace();
            }


        });
    }

    private List<MonAn> loaddulieu() throws RemoteException, NotBoundException {
        Services services = Services.getInstance();
        IMonAnDAO iMonAnDAO = services.getMonAnService();
        List<MonAn> list = iMonAnDAO.find();
        //list= iMonAnDAO.find();
        return list;
    }

    public String getMaBanHienTai(){
        return hoaDonban.getBan().getMaBan();
    }
}
