package org.buffalocoder.quanlyquancafe.client.controller.nhanvien;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.buffalocoder.quanlyquancafe.client.controller.AlertController;
import org.buffalocoder.quanlyquancafe.client.ultis.Component;
import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.client.ultis.StorageAccount;
import org.buffalocoder.quanlyquancafe.common.entities.ChiTietHoaDon;
import org.buffalocoder.quanlyquancafe.common.entities.HoaDon;
import org.buffalocoder.quanlyquancafe.common.exceptions.NotExistException;
import org.buffalocoder.quanlyquancafe.common.interfaces.IHoaDonDAO;

import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class HomeController implements Initializable {
    private AlertController alertController;
    private IHoaDonDAO hoaDonDAO;
    private Set<ChiTietHoaDon> chiTietHoaDons;
    private Set<String> chiTietMaBan;
    private List<String> chiTietMaBans;
    private HoaDon hoaDon;
    private String maBanMuonThanhToan;
    private Component component;
    private Parent root;

    @FXML
    private Label lblTenNguoiDung, lblTenNhanVien;
    @FXML
    private StackPane stackPane;
    @FXML
    private Pane pane_Tong;
    @FXML
    private JFXButton btn_SodoMonan, btn_TatcaBan, btn_ThongKe, btn_DoiMatKhau;
    @FXML
    private Pane pane_ThongKe;
    @FXML
    private GridPane GridPaneDatMon;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertController = new AlertController(stackPane);

        try {
            hoaDonDAO = Services.getInstance().getHoaDonService();
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }

        try {
            lblTenNguoiDung.setText(StorageAccount.getInstance().getNhanVien().getTenNhanVien());
            lblTenNhanVien.setText(StorageAccount.getInstance().getNhanVien().getTenNhanVien());
        } catch (NotExistException e) {
            alertController.alertError(e.getMessage());
        }

        chiTietMaBans = new ArrayList<>();
        maBanMuonThanhToan = "123";
        component = new Component();
        chiTietHoaDons = new HashSet<>();
        chiTietMaBan = new HashSet<>();
        hoaDon = new HoaDon();
        loadSoDoBanScene();
    }

    // region Events
    /**
     * Phương thức xử lý khi admin nhấn nút quản lý
     * @param event
     * @throws IOException
     */
    @FXML
    private void onClickQuanLy(ActionEvent event) throws IOException {
        try {
            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            stage.close();
            openMainQuanLy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Phương thức xử lý khi nhấn nút đăng xuất
     * @param actionEvent
     */
    @FXML
    public void onClickDangXuat(ActionEvent actionEvent) {
        StorageAccount.getInstance().setNhanVien(null);
        try {
            Node node = (Node) actionEvent.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            stage.close();

            Parent root = FXMLLoader.load(getClass().getResource("/fxml/nhanvien/DangNhapScene.fxml"));
            root.getStylesheets().add(getClass().getResource("/css/common.css").toExternalForm());
            Scene scene = new Scene(root, 1067, 712);
            stage = new Stage();
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // endregion

    // region Methods

    /**
     * Phương thức xử lý mở màn hình quản lý
     *
     * @throws IOException
     */
    private void openMainQuanLy() {
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/admin/QuanLyScene.fxml"));
            root.getStylesheets().add(getClass().getResource("/css/common.css").toExternalForm());
            Scene scene = new Scene(root, 1380, 760);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // endregion

    @FXML
    private void handleButtonMouse(MouseEvent event) throws IOException {
        if (event.getSource() == btn_SodoMonan) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/nhanvien/SodoMonan.fxml"));
            root = loader.load();

            SodoMonanController sodoMonanController = loader.getController();
            sodoMonanController.setIndex(1);
            sodoMonanController.setGridPaneDatMon(GridPaneDatMon);
            sodoMonanController.setChiTietHoaDons(chiTietHoaDons);

            if (GridPaneDatMon != null) {
                System.out.println("ko bị null");
            }
            pane_Tong.getChildren().removeAll();
            pane_Tong.getChildren().setAll(root);

            Parent rootMonan = FXMLLoader.load(getClass().getResource("/fxml/nhanvien/MonAnFilter.fxml"));
            return;
        } else if (event.getSource() == btn_TatcaBan) {
//            root = FXMLLoader.load(getClass().getResource("/fxml/nhanvien/SoDoBanScene.fxml"));
//            root.getStylesheets().add(getClass().getResource("/css/common.css").toExternalForm());
//
//
//            pane_Tong.getChildren().removeAll();
//            pane_Tong.getChildren().setAll(root);
            try {
                Set<Node> deleteNodes = new HashSet<>(GridPaneDatMon.getChildren());
                GridPaneDatMon.getChildren().removeAll(deleteNodes);

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/fxml/nhanvien/SoDoBanScene.fxml"));
                root = loader.load();

                chiTietMaBans = new ArrayList<>();
                chiTietMaBan = new HashSet<>();
                SoDoBanController soDoBanController = loader.getController();
                soDoBanController.setChiTietHoaDons(chiTietHoaDons);
                soDoBanController.setGridPaneDatMon(GridPaneDatMon);
                soDoBanController.setChiTietMaBan(chiTietMaBan);
                soDoBanController.setMaBanMuonThanhToan(maBanMuonThanhToan);
                soDoBanController.setChiTietMaBans(chiTietMaBans);
//            root = FXMLLoader.load(getClass().getResource("/fxml/nhanvien/SoDoB
//
            } catch (IOException e) {
                e.printStackTrace();
            }
            pane_Tong.getChildren().removeAll();
            pane_Tong.getChildren().setAll(root);
            return;
        } else if (event.getSource() == btn_ThongKe) {
            root = FXMLLoader.load(getClass().getResource("/fxml/nhanvien/ThongKeScene.fxml"));
            root.getStylesheets().add(getClass().getResource("/css/common.css").toExternalForm());
            pane_ThongKe.getChildren().removeAll();
            pane_ThongKe.getChildren().setAll(root);
            return;
        } else if (event.getSource() == btn_DoiMatKhau) {
            root = FXMLLoader.load(getClass().getResource("/fxml/nhanvien/DoiMatKhau.fxml"));
            pane_ThongKe.getChildren().removeAll();
            pane_ThongKe.getChildren().setAll(root);
            return;
        }
    }

    @FXML
    private void moThanhToan(MouseEvent event) {

        try {
            HoaDon hoaDon = hoaDonDAO.findHoaDonChuaThanhToanByMaBan(chiTietMaBans.get(0));

            if (hoaDon == null) return;

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/nhanvien/popup/ThanhToanScene.fxml"));
            Parent root = loader.load();

            ThanhToanController thanhToanController = loader.getController();
            thanhToanController.setPane(pane_Tong);
            thanhToanController.setGridPaneDatMon(GridPaneDatMon);
            thanhToanController.setHoaDon(hoaDon);

            Scene scene = new Scene(root, 565, 587);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initOwner(((Node)(event.getSource())).getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    // region Methods

    /**
     * Phương thức xử lý load dữ liệu sơ đồ bàn
     */
    private void loadSoDoBanScene() {
        chiTietMaBan = new HashSet<>();
        Parent root = null;
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/nhanvien/SoDoBanScene.fxml"));
            root = loader.load();

            SoDoBanController soDoBanController = loader.getController();
            soDoBanController.setChiTietHoaDons(chiTietHoaDons);
            soDoBanController.setGridPaneDatMon(GridPaneDatMon);
            soDoBanController.setHoaDonBan(hoaDon);
            soDoBanController.setMaBanMuonThanhToan(maBanMuonThanhToan);
            soDoBanController.setChiTietMaBan(chiTietMaBan);
            soDoBanController.setChiTietMaBans(chiTietMaBans);

        } catch (IOException e) {
            e.printStackTrace();
        }

        pane_Tong.getChildren().removeAll();
        pane_Tong.getChildren().setAll(root);
    }
    // endregion
}
