package org.buffalocoder.quanlyquancafe.client;

import org.buffalocoder.quanlyquancafe.client.ultis.Services;
import org.buffalocoder.quanlyquancafe.client.views.DangNhapScene;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Main {
    public static void main(String[] args) {
        try {
            Services.getInstance();
            DangNhapScene.main(args);
        } catch (RemoteException | NotBoundException e) {
            System.err.println("Không thể kết nối đến server");
            System.exit(1);
        }
    }
}
