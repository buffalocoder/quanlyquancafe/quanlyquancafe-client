
package org.buffalocoder.quanlyquancafe.client.ultis;



import org.buffalocoder.quanlyquancafe.common.interfaces.*;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Lớp connect service DAO server
 *
 * @author Nguyễn Chí Cường
 * @since 06-10-2019
 * @version 1.0
 */
public class Services {
    private static final String HOST = "nguyenchicuong-y530";
    private static final int PORT = 9999;

    private static Services _instance;
    private static Registry registry;

    private static IBanDAO banService;

    private static IDanhMucMonAnDAO danhMucMonAnService;

    private static IHoaDonDAO hoaDonService;

    private static IMonAnDAO monAnService;

    private static INhanVienDAO nhanVienService;


    public IBanDAO getBanService() {
        return banService;
    }

    public IDanhMucMonAnDAO getDanhMucMonAnService() {
        return danhMucMonAnService;
    }

    public IHoaDonDAO getHoaDonService() {
        return hoaDonService;
    }

    public IMonAnDAO getMonAnService() {
        return monAnService;
    }

    public INhanVienDAO getNhanVienService() {
        return nhanVienService;
    }

    private Services() throws RemoteException, NotBoundException {
        registry = LocateRegistry.getRegistry(HOST, PORT);

        banService = (IBanDAO) registry.lookup(IBanDAO.class.getSimpleName());
        danhMucMonAnService = (IDanhMucMonAnDAO) registry.lookup(IDanhMucMonAnDAO.class.getSimpleName());
        hoaDonService = (IHoaDonDAO) registry.lookup(IHoaDonDAO.class.getSimpleName());
        monAnService = (IMonAnDAO) registry.lookup(IMonAnDAO.class.getSimpleName());
        nhanVienService = (INhanVienDAO) registry.lookup(INhanVienDAO.class.getSimpleName());
    }

    public static Services getInstance() throws RemoteException, NotBoundException {
        if (_instance == null) {
            synchronized (Services.class) {
                if (null == _instance) {
                    _instance = new Services();
                }
            }
        }
        return _instance;
    }
}
