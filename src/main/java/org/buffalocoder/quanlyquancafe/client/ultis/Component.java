package org.buffalocoder.quanlyquancafe.client.ultis;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import org.buffalocoder.quanlyquancafe.common.entities.Ban;
import org.buffalocoder.quanlyquancafe.common.interfaces.IBanDAO;

import java.io.File;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;


public class Component {
    private List<Ban> banList;
    private int index = 0;
    private IBanDAO banDAO;
    public Pane getPaneMonAn(String tenMonAn,double giaBan) throws Exception {
        Pane pane = new Pane();
        pane.setPrefSize(160,193);
        Label tenLabel= new Label(tenMonAn);
        Label GiaLabel = new Label(String.valueOf(giaBan));

        tenLabel.setLayoutX(45);
        tenLabel.setLayoutY(129);

        GiaLabel.setLayoutX(61);
        GiaLabel.setLayoutY(157);

        pane.getChildren().addAll(tenLabel,GiaLabel);


        File file = new File(getClass().getResource("/images/cafe.jpg").getPath());
        String localUrl = file.toURI().toURL().toString();
        Image image = new Image(localUrl);
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(115);
        imageView.setFitWidth(126);
        imageView.setLayoutX(17);
        imageView.setLayoutY(14);

        pane.getChildren().add(imageView);
        return pane;
    }
    public Label getBan(int viTri, String id,boolean daDat) {
        Label banLabel = new Label();
        banLabel.setId(id);
        banLabel.setOnMouseClicked(event -> {
            index = 0;
            try {
                banDAO = Services.getInstance().getBanService();
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (NotBoundException e) {
                e.printStackTrace();
            }
            banList = null;
            try {
                banList = banDAO.find();
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            banList.forEach(ban -> {
                index++;
                Scene scene = banLabel.getScene();
                Label labelBanOlder = (Label) scene.lookup("#lblBan" + String.valueOf(index));
                labelBanOlder.setStyle("-fx-background-color: white; -fx-border-color: #008000; -fx-border-width: 0 0 4 0;");
            });
            banLabel.setStyle("-fx-background-color: white; -fx-border-color: #0277BD; -fx-border-width: 3 3 4 3;");
        });
        banLabel.setText(String.valueOf(viTri));
        banLabel.setPrefWidth(120);
        banLabel.setPrefHeight(100);
        if(!daDat){
            banLabel.setStyle("-fx-background-color: white; -fx-border-color: #008000; -fx-border-width: 0 0 4 0;");
        }
        else {
            banLabel.setStyle("-fx-background-color: #56A5EC; -fx-border-color: #008000; -fx-border-width: 0 0 4 0;");
        }

        //banLabel.setStyle("-fx-background-color: white; -fx-border-color: #008000; -fx-border-width: 0 0 4 0;");
        banLabel.setAlignment(Pos.CENTER);

        return banLabel;

    }

    public Pane getPaneDatMon (String maMon,String tenMon,int soLuong,double giaBan,double thanhTien) throws MalformedURLException {
        Pane paneDatMon = new Pane();
        paneDatMon.setPrefWidth(200);
        paneDatMon.setPrefHeight(200);
        paneDatMon.setStyle("-fx-border-color: black; -fx-border-width: 1 0 1 0;");
        Label labelSoLuong = new Label(String.valueOf(soLuong));
        Label labelMaMon = new Label(maMon);
        Label labelTenMon = new Label(tenMon);
        Label labelGiaTien = new Label(String.valueOf(giaBan));
        Label labelTongTien = new Label(String.valueOf(thanhTien));

        labelSoLuong.setLayoutX(29);
        labelSoLuong.setLayoutY(15);
        labelSoLuong.setFont(new Font(25));

        labelMaMon.setLayoutX(59);
        labelMaMon.setLayoutY(34);
        labelMaMon.setFont(new Font(14));


        labelTenMon.setLayoutX(59);
        labelTenMon.setLayoutY(15);
        labelTenMon.setFont(new Font(14));


        labelGiaTien.setLayoutX(206);
        labelGiaTien.setLayoutY(24);
        labelGiaTien.setFont(new Font(14));

        labelTongTien.setLayoutX(278);
        labelTongTien.setLayoutY(24);
        labelTongTien.setFont(new Font(14));


        paneDatMon.getChildren().addAll(labelSoLuong,labelMaMon,labelTenMon,labelGiaTien,labelTongTien);


        return paneDatMon;

//        Pane pane = new Pane();
//        pane.setPrefSize(200,200);
//        Label tenLabel= new Label("123");
//        Label GiaLabel = new Label(String.valueOf(giaBan));
//
//        tenLabel.setLayoutX(45);
//        tenLabel.setLayoutY(129);
//
//        GiaLabel.setLayoutX(61);
//        GiaLabel.setLayoutY(157);
//
//        pane.getChildren().addAll(tenLabel,GiaLabel);
//
////        File file = new File("/images/cafe.jpg");
////        String localUrl = file.toURI().toURL().toString();
////        Image image = new Image(localUrl);
////        ImageView imageView = new ImageView(image);
////        imageView.setFitHeight(115);
////        imageView.setFitWidth(126);
////        imageView.setLayoutX(17);
////        imageView.setLayoutY(14);
//
//       // pane.getChildren().add(imageView);
//        return pane;
    }
}
