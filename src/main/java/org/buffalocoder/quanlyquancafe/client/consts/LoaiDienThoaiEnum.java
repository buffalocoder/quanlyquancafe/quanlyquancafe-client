package org.buffalocoder.quanlyquancafe.client.consts;

public enum LoaiDienThoaiEnum {
    DI_DONG,
    NHA_RIENG,
    CO_QUAN,
    KHAC
}
